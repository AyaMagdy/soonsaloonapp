//
//  RequestAPI.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/30/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation

import Alamofire

class RequestAPI {
    
    static let shared = RequestAPI()
    
    private let BASE_URL = "http://www.wshwshaa.com/soonsaloon/public/api"

    private let PUBLIC_KEY = "publicKey"
    private let AUTH_KEY = "Authorization"
    private let BEARE_KEY = "Bearer "
    private let PUBLIC_KEY_VALUE = "1yFhFvok4FPK43rU3IJTq/c4vvxItEVvm4uQ5KQgbfo"
    private var MAIN_HEADER : [String:String]?

    private var uType = ""
    
    private init() {
    }
    
    private func getHeader() -> [String:String]{
        // MAIN_HEADER = [PUBLIC_KEY:PUBLIC_KEY_VALUE]
        //return MAIN_HEADER!
       return  [
            "publicKey": "1yFhFvok4FPK43rU3IJTq/c4vvxItEVvm4uQ5KQgbfo",
            "Authorization": "Bearer \(TokenCache.get())"
        ]
    }
    
    func login(phone:String,password:String,userType:UserType,completion: @escaping (ApiResult<UserResponse?>) -> Void){
        if userType == .saloon{
            uType = "saloon"
        }
        else{
            uType = "customer"
        }
        let url = "\(BASE_URL)/\(uType)/login?lang=\(getLang())"
        let params = ["phone":phone,"password":password]
        
        Alamofire.request(url, method: .post, parameters: params, headers: getHeader())
            .responseJSON{(response) in
                switch response.result {
                case .success(_):
                    if let data = response.data {
                        do {
                            let dataResponse = try JSONDecoder().decode(BaseResponse<UserResponse>.self, from: data)
                            if dataResponse.status {
                                completion(.success(dataResponse.data))
                            }else {
                                completion(.failure(dataResponse.message))
                            }
                        }catch let e {
                            completion(.failure(e.localizedDescription))
                        }
                    }
                    break
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
            }.responseString{ response in
                print(response)
        }
        
    }
    func saloonRegister(profile_image: UIImage , password : String , userObj : User ,completion: @escaping (ApiResult<UserResponse?>) -> Void)
    {
        
        let url = "\(BASE_URL)/saloon/sign-up?lang=\(getLang())"
        var params = [String:String]()
        params["phone"] = userObj.phone
        params["password"] = password
        params["password_confirmation" ] = password
        params["name"] = userObj.name
        params["country"] = userObj.country
        params["city"] = userObj.city
        params["opening_time_from"] = userObj.model?.openingTimeFrom
        params["opening_time_to"] = userObj.model?.openingTimeTo
        params["opening_day_from"] = "\(userObj.model?.openingDayFrom ?? 0)"
        params["opening_day_to"] = "\(userObj.model?.openingDayTo ?? 0)"
        
        let imgData = profile_image.jpegData(compressionQuality: 1.5)
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            //Parameter for Upload files
            multipartFormData.append(imgData!, withName: "photo",fileName: "furkan.png" , mimeType: "image/png")
            for (key, value) in params
            {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, usingThreshold:UInt64.init(),
           to: url, //URL Here
            method: .post,
            headers: getHeader(),
            encodingCompletion: { (result) in
                switch result {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                    })
                    upload.responseString { response in
                        print(response)
                        switch response.result {
                        case .success(_):
                            if let data = response.data {
                                do {
                                    let dataResponse = try JSONDecoder().decode(BaseResponse<UserResponse>.self, from: data)
                                    if dataResponse.status {
                                        completion(.success(dataResponse.data))
                                    }else {
                                        completion(.failure(dataResponse.message))
                                    }
                                }catch let e {
                                    completion(.failure(e.localizedDescription))
                                }
                            }
                            break
                        case let .failure(error):
                            completion(.failure(error.localizedDescription))
                        }
                    }.responseString{ response in
                        print(response)
                    }
                    break
                case .failure(let error):
                    completion(.failure(error.localizedDescription))
                    break
                }
        })
    }
    
    
    
    func allCities(CountryId:Int,completion: @escaping (ApiResult<[City]?>) -> Void){
        let url = "\(BASE_URL)/cities/\(CountryId)?lang=\(getLang())"
        Alamofire.request(url, method:.get,headers: getHeader())
            .responseJSON{(response) in
                switch response.result {
                case .success(_):
                    if let data = response.data {
                        do {
                            let dataResponse = try JSONDecoder().decode(BaseResponseArray<City>.self, from: data)
                            if dataResponse.status {
                                completion(.success(dataResponse.data))
                            }else {
                                completion(.failure(dataResponse.message))
                            }
                        }catch let e {
                            completion(.failure(e.localizedDescription))
                        }
                    }
                    break
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }.responseString{ reponse in
        print(reponse)
        }
        
    }
    
    
    func getCountries(completion: @escaping (ApiResult<[City]?>) -> Void){
        let url = "\(BASE_URL)/countries?lang=\(getLang())"
        Alamofire.request(url, method:.get,headers: getHeader())
            .responseJSON{(response) in
                switch response.result {
                case .success(_):
                    if let data = response.data {
                        do {
                            let dataResponse = try JSONDecoder().decode(BaseResponseArray<City>.self, from: data)
                            if dataResponse.status {
                                completion(.success(dataResponse.data))
                            }else {
                                completion(.failure(dataResponse.message))
                            }
                        }catch let e {
                            completion(.failure(e.localizedDescription))
                        }
                    }
                    break
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
            }.responseString{ reponse in
                print(reponse)
        }
        
    }
    
    func getSaloonSevice(completion: @escaping (ApiResult<ServiceResponse?>) -> Void){
        let url = "\(BASE_URL)/saloon/service?lang=\(getLang())"
        Alamofire.request(url, method: .get,headers: getHeader())
            .responseString{(response) in
                print(response)
                switch response.result {
                case .success(_):
                    if let data = response.data {
                        do {
                            let dataResponse = try JSONDecoder().decode(BaseResponse<ServiceResponse>.self, from: data)
                            if dataResponse.status {
                                completion(.success(dataResponse.data))
                            }else {
                                completion(.failure(dataResponse.message))
                            }
                        }catch let e {
                            completion(.failure(e.localizedDescription))
                        }
                    }
                    break
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
        
    }
    
    func getSaloonSeviceDetails(serviceId : Int,completion: @escaping (ApiResult<SaloonService?>) -> Void){
        let url = "\(BASE_URL)/service/\(serviceId)?lang=" + getLang()
        Alamofire.request(url, method: .get,headers: getHeader())
            .responseString{(response) in
                switch response.result {
                case .success(_):
                    if let data = response.data {
                        do {
                            let dataResponse = try JSONDecoder().decode(BaseResponse<SaloonService>.self, from: data)
                            if dataResponse.status {
                                completion(.success(dataResponse.data))
                            }else {
                                completion(.failure(dataResponse.message))
                            }
                        }catch let e {
                            completion(.failure(e.localizedDescription))
                        }
                    }
                    break
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func allNotifications(completion: @escaping (ApiResult<[Notification]?>) -> Void){
        let url = "\(BASE_URL)/request/notification?lang=" + getLang()
        Alamofire.request(url, method:.get,headers: getHeader())
            .responseJSON{(response) in
                switch response.result {
                case .success(_):
                    if let data = response.data {
                        do {
                            let dataResponse = try JSONDecoder().decode(BaseResponseArray<Notification>.self, from: data)
                            if dataResponse.status {
                                completion(.success(dataResponse.data))
                            }else {
                                completion(.failure(dataResponse.message))
                            }
                        }catch let e {
                            completion(.failure(e.localizedDescription))
                        }
                    }
                    break
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func getCategoriesAndSubCategories(completion: @escaping (ApiResult<[MainCategory]?>) -> Void){
        let url = "\(BASE_URL)/categories?lang=" + getLang()
        Alamofire.request(url, method: .get,headers: getHeader())
            .responseJSON{(response) in
                switch response.result {
                case .success(_):
                    if let data = response.data {
                        do {
                            let dataResponse = try JSONDecoder().decode(BaseResponse<[MainCategory]>.self, from: data)
                            if dataResponse.status {
                                completion(.success(dataResponse.data))
                            }else {
                                completion(.failure(dataResponse.message))
                            }
                        }catch let e {
                            completion(.failure(e.localizedDescription))
                        }
                    }
                    break
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
        
    }
    
    func getBanner(completion: @escaping (ApiResult<[Banner]?>) -> Void){
        let url = "\(BASE_URL)/banner?lang=" + getLang()
        Alamofire.request(url, method: .get,headers: getHeader())
            .responseJSON{(response) in
                switch response.result {
                case .success(_):
                    if let data = response.data {
                        do {
                            let dataResponse = try JSONDecoder().decode(BaseResponse<[Banner]>.self, from: data)
                            if dataResponse.status {
                                completion(.success(dataResponse.data))
                            }else {
                                completion(.failure(dataResponse.message))
                            }
                        }catch let e {
                            completion(.failure(e.localizedDescription))
                        }
                    }
                    break
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
        
    }
    
    func about(completion: @escaping (ApiResult<String>) -> Void) {
        let url = "\(BASE_URL)/about-us?lang=\(getLang())"
        
        Alamofire.request(url , method : .get,headers: getHeader())
            .responseJSON { (response) in
                switch response.result {
                case .success:
                    let jsonData = response.data
                    do{
                        let dataResponse = try JSONDecoder().decode(BaseResponseString<About>.self, from: jsonData!)
                        if dataResponse.status {
                            completion(.success(dataResponse.data))
                        } else {
                            completion(.failure(dataResponse.message))
                        }
                    }catch {
                        completion(.failure(error.localizedDescription))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func getLang() -> String {
        return "en"
    }
    
    func updateUser(name: String,phone : String , city : Int , country : Int, dayFrom :Int , dayTo : Int , timeFrom : String , timeto : String , photo : UIImage ,latitude:Double,longitude:Double,completion: @escaping (ApiResult<User?>) -> Void){
        print(photo)
        let url = "\(BASE_URL)/profile?lang=\(getLang())"
        var params = [String:String]()
        params["phone"] = phone
        params["name"] = name
        params["country"] = String(country)
        params["city"] = String(city)
        params["opening_time_from"] = timeFrom
        params["opening_time_to"] = timeto
        params["opening_day_from"] = "\(dayFrom)"
        params["opening_day_to"] = "\(dayTo)"
        params["latitude"] = "\(latitude)"
        params["longitude"] = "\(longitude)"
        
        let imgData = photo.jpegData(compressionQuality: 1.5)
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            //Parameter for Upload files
            multipartFormData.append(imgData!, withName: "profile_image",fileName: "furkan.png" , mimeType: "image/png")
            for (key, value) in params
            {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, usingThreshold:UInt64.init(),
           to: url, //URL Here
            method: .post,
            headers: getHeader(),
            encodingCompletion: { (result) in
                switch result {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                    })
                    upload.responseString { response in
                        print(response)
                        switch response.result {
                        case .success(_):
                            if let data = response.data {
                                do {
                                    let dataResponse = try JSONDecoder().decode(BaseResponse<User>.self, from: data)
                                    print(dataResponse)
                                    if dataResponse.status {
                                        completion(.success(dataResponse.data))
                                    }else {
                                        completion(.failure(dataResponse.message))
                                    }
                                }catch let e {
                                    completion(.failure(e.localizedDescription))
                                }
                            }
                            break
                        case let .failure(error):
                            completion(.failure(error.localizedDescription))
                        }
                    }
                    break
                case .failure(let error):
                    completion(.failure(error.localizedDescription))
                    break
                }
        })
    }
    
    func updateUser(name: String,phone : String , city : Int , country : Int, photo : UIImage ,completion: @escaping (ApiResult<User?>) -> Void){
        print(photo)
        let url = "\(BASE_URL)/profile?lang=\(getLang())"
        var params = [String:String]()
        params["phone"] = phone
        params["name"] = name
        params["country"] = String(country)
        params["city"] = String(city)
        params["gender"] = String(UserCache.get()?.model?.gender ?? 0)
        
        let imgData = photo.jpegData(compressionQuality: 1.5)
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            //Parameter for Upload files
            multipartFormData.append(imgData!, withName: "profile_image",fileName: "furkan.png" , mimeType: "image/png")
            for (key, value) in params
            {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, usingThreshold:UInt64.init(),
           to: url, //URL Here
            method: .post,
            headers: getHeader(),
            encodingCompletion: { (result) in
                switch result {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                    })
                    upload.responseString { response in
                        print(response)
                        switch response.result {
                        case .success(_):
                            if let data = response.data {
                                do {
                                    let dataResponse = try JSONDecoder().decode(BaseResponse<User>.self, from: data)
                                    print(dataResponse)
                                    if dataResponse.status {
                                        completion(.success(dataResponse.data))
                                    }else {
                                        completion(.failure(dataResponse.message))
                                    }
                                }catch let e {
                                    completion(.failure(e.localizedDescription))
                                }
                            }
                            break
                        case let .failure(error):
                            completion(.failure(error.localizedDescription))
                        }
                    }
                    break
                case .failure(let error):
                    completion(.failure(error.localizedDescription))
                    break
                }
        })
    }
    
    func addService(description: String,seats : String , subCategory : String , photos : [UIImage] ,price : String ,completion: @escaping (ApiResult<SaloonService?>) -> Void){
        print(photos)
        let url = "\(BASE_URL)/saloon/service/store?lang=\(getLang())"
        var params = [String:String]()
        params["description"] = description
        params["seats"] = seats
        params["subCategoryId"] = subCategory
        params["price"] = price
        
        Alamofire.upload(multipartFormData: { multipartFormData in
            //Parameter for Upload files
            for photo in photos {
                let imageData = photo.jpegData(compressionQuality: 1.5)
                multipartFormData.append(imageData!, withName: "image[]", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            }
            
            for (key, value) in params
            {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
        }, usingThreshold:UInt64.init(),
           to: url, //URL Here
            method: .post,
            headers: getHeader(),
            encodingCompletion: { (result) in
                switch result {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                    })
                    upload.responseString { response in
                        print(response)
                        switch response.result {
                        case .success(_):
                            if let data = response.data {
                                do {
                                    let dataResponse = try JSONDecoder().decode(BaseResponse<SaloonService>.self, from: data)
                                    print(dataResponse)
                                    if dataResponse.status {
                                        completion(.success(dataResponse.data))
                                    }else {
                                        completion(.failure(dataResponse.message))
                                    }
                                }catch let e {
                                    completion(.failure(e.localizedDescription))
                                }
                            }
                            break
                        case let .failure(error):
                            completion(.failure(error.localizedDescription))
                        }
                    }
                    break
                case .failure(let error):
                    completion(.failure(error.localizedDescription))
                    break
                }
        })
    }
    
    func getRequestHistory(completion: @escaping (ApiResult<[Request]?>) -> Void){
        let url = "\(BASE_URL)/saloon/history/requests?lang=\(getLang())"
        print(TokenCache.get())
        Alamofire.request(url, method: .get , headers: getHeader())
            .responseJSON{(response) in
                print(response)
                switch response.result {
                case .success(_):
                    if let data = response.data {
                        print(data)
                        do {
                            let dataResponse = try JSONDecoder().decode(BaseResponse<[Request]>.self, from: data)
                            if dataResponse.status {
                                completion(.success(dataResponse.data))
                            }else {
                                completion(.failure(dataResponse.message))
                            }
                        }catch let e {
                            completion(.failure(e.localizedDescription))
                        }
                    }
                    break
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
        
    }
    
    func getRequestCurrent(completion: @escaping (ApiResult<[Request]?>) -> Void){
        let url = "\(BASE_URL)/saloon/current/requests?lang=" + getLang()
        Alamofire.request(url, method: .get , headers: getHeader())
            .responseJSON{(response) in
                print(response)
                switch response.result {
                case .success(_):
                    if let data = response.data {
                        print(data)
                        do {
                            let dataResponse = try JSONDecoder().decode(BaseResponse<[Request]>.self, from: data)
                            if dataResponse.status {
                                completion(.success(dataResponse.data))
                            }else {
                                completion(.failure(dataResponse.message))
                            }
                        }catch let e {
                            completion(.failure(e.localizedDescription))
                        }
                    }
                    break
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
        
    }
    
    func getSubCategories(CategoryId:Int,completion: @escaping (ApiResult<[SubCategories]?>) -> Void){
        let url = "\(BASE_URL)/sub_categories/\(CategoryId)?lang=" + getLang()
        Alamofire.request(url, method: .get,headers: getHeader())
            .responseJSON{(response) in
                switch response.result {
                case .success(_):
                    if let data = response.data {
                        do {
                            let dataResponse = try JSONDecoder().decode(BaseResponse<[SubCategories]>.self, from: data)
                            if dataResponse.status {
                                completion(.success(dataResponse.data))
                            }else {
                                completion(.failure(dataResponse.message))
                            }
                        }catch let e {
                            completion(.failure(e.localizedDescription))
                        }
                    }
                    break
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
        
    }
    
    func cancelRequest(requestId : Int ,completion: @escaping (ApiResult<Request?>) -> Void) {
        let url = "\(BASE_URL)/request/cancel-status?lang=\(getLang())"
        let params = ["request_id" : requestId]
        Alamofire.request(url, method: .post, parameters: params, headers: getHeader())
            .responseJSON { (response) in
                switch response.result {
                case .success:
                    let jsonData = response.data
                    do{
                        let dataResponse = try JSONDecoder().decode(BaseResponse<Request>.self, from: jsonData!)
                        if dataResponse.status {
                            completion(.success(dataResponse.data))
                        } else {
                            completion(.failure(dataResponse.message))
                        }
                    }catch {
                        completion(.failure(error.localizedDescription))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func changeRequestStatus(requestId : Int , statusCode : Int ,completion: @escaping (ApiResult<Request?>) -> Void) {
        let url = "\(BASE_URL)/saloon/change/request/status?lang=\(getLang())"
        let params = ["request_id" : requestId , "status_code" : statusCode ]
        Alamofire.request(url, method: .post, parameters: params, headers: getHeader())
            .responseJSON { (response) in
                switch response.result {
                case .success:
                    let jsonData = response.data
                    do{
                        let dataResponse = try JSONDecoder().decode(BaseResponse<Request>.self, from: jsonData!)
                        if dataResponse.status {
                            completion(.success(dataResponse.data))
                        } else {
                            completion(.failure(dataResponse.message))
                        }
                    }catch {
                        completion(.failure(error.localizedDescription))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func editRequest(requestId : Int , date : String ,time :String ,completion: @escaping (ApiResult<Request?>) -> Void) {
        print("edit request")
        let url = "\(BASE_URL)/request/update?lang=\(getLang())"
        let params = ["id" : requestId , "date" : date , "time" :time ] as [String : Any]
        Alamofire.request(url, method: .post, parameters: params, headers: getHeader())
            .responseJSON { (response) in
                switch response.result {
                case .success:
                    let jsonData = response.data
                    
                    do{
                        let dataResponse = try JSONDecoder().decode(BaseResponse<Request>.self, from: jsonData!)
                        print(dataResponse)
                        if dataResponse.status {
                            completion(.success(dataResponse.data))
                        } else {
                            completion(.failure(dataResponse.message))
                        }
                    }catch {
                        completion(.failure(error.localizedDescription))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func getServices(SubCategoryId:Int,completion: @escaping (ApiResult<[SaloonService]?>) -> Void){
        let url = "\(BASE_URL)/services/\(SubCategoryId)?lang=" + getLang()
        Alamofire.request(url, method: .get,headers: getHeader())
            .responseJSON{(response) in
                switch response.result {
                case .success(_):
                    if let data = response.data {
                        do {
                            let dataResponse = try JSONDecoder().decode(BaseResponse<[SaloonService]>.self, from: data)
                            if dataResponse.status {
                                completion(.success(dataResponse.data))
                            }else {
                                completion(.failure(dataResponse.message))
                            }
                        }catch let e {
                            completion(.failure(e.localizedDescription))
                        }
                    }
                    break
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func changePassword(oldPassword: String,newPassword : String,completion: @escaping (ApiResult<String>) -> Void){
        let url = "\(BASE_URL)/change-password?lang=\(getLang())"
        let params = ["current_password" : oldPassword , "password" : newPassword]
        
        Alamofire.request(url, method: .post, parameters: params, headers: getHeader())
            .responseJSON{(response) in
                //                print(response)
                switch response.result {
                case .success(_):
                    if let data = response.data {
                        print(data)
                        do {
                            print("11111")
                            let dataResponse = try JSONDecoder().decode(PureResponse.self, from: data)
                            print(dataResponse)
                            if dataResponse.status {
                                completion(.success(dataResponse.message))
                            }else {
                                completion(.failure(dataResponse.message))
                            }
                        }catch let e {
                            print("catch")
                            completion(.failure(e.localizedDescription))
                        }
                    }
                    break
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func userRegister(password_confirmation : String ,password : String , userObj : User ,completion: @escaping (ApiResult<UserResponse?>) -> Void){
        let url = "\(BASE_URL)/customer/sign-up?lang=\(getLang())"
        var params = [String:String]()
        params["phone"] = userObj.phone
        params["password"] = password
        params["password_confirmation" ] = password_confirmation
        params["name"] = userObj.name
        params["country"] = userObj.country
        params["city"] = userObj.city
        params["gender"] = "\(userObj.model?.gender ?? 0)"

        Alamofire.request(url, method: .post, parameters: params, headers: getHeader())
            .responseJSON{(response) in
                print(response)
                switch response.result {
                case .success(_):
                    if let data = response.data {
                        do {
                            let dataResponse = try JSONDecoder().decode(BaseResponse<UserResponse>.self, from: data)
                            print(dataResponse)
                            if dataResponse.status {
                                completion(.success(dataResponse.data))
                            }else {
                                completion(.failure(dataResponse.message))
                            }
                        }catch let e {
                            completion(.failure(e.localizedDescription))
                        }
                    }
                    break
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
        
    }
    
    func deleteService(serviceId : Int ,completion: @escaping (ApiResult<String>) -> Void) {
        let url = "\(BASE_URL)/saloon/service/destroy/\(serviceId)?lang=\(getLang())"
        Alamofire.request(url, method: .get, headers: getHeader())
            .responseJSON { (response) in
                switch response.result {
                case .success:
                    let jsonData = response.data
                    do{
                        let dataResponse = try JSONDecoder().decode(PureResponse.self, from: jsonData!)
                        if dataResponse.status {
                            completion(.success(dataResponse.message))
                        } else {
                            completion(.failure(dataResponse.message))
                        }
                    }catch {
                        completion(.failure(error.localizedDescription))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
            }
    }
    
    func addRequestRate(requestId : Int , comment : String ,rate :Int ,completion: @escaping (ApiResult<Comment?>) -> Void) {
        print("edit request")
        let url = "\(BASE_URL)/rating/create?lang=" + getLang()
        let params = ["model_id" : requestId , "comment" : comment , "rate" : rate ,"model_type" : "request" ] as [String : Any]
        Alamofire.request(url, method: .post, parameters: params, headers: getHeader())
            .responseJSON { (response) in
                switch response.result {
                case .success:
                    let jsonData = response.data
                    
                    do{
                        let dataResponse = try JSONDecoder().decode(BaseResponse<Comment>.self, from: jsonData!)
                        print(dataResponse)
                        if dataResponse.status {
                            completion(.success(dataResponse.data))
                        } else {
                            completion(.failure(dataResponse.message))
                        }
                    }catch {
                        completion(.failure(error.localizedDescription))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
   
    func logout(completion: @escaping (ApiResult<String>) -> Void) {
        let url = "\(BASE_URL)/auth/logout?lang=\(getLang())"
        Alamofire.request(url, method: .get, headers: getHeader())
            .responseJSON { (response) in
                switch response.result {
                case .success:
                    let jsonData = response.data
                    do{
                        let dataResponse = try JSONDecoder().decode(PureResponse.self, from: jsonData!)
                        if dataResponse.status {
                            completion(.success(dataResponse.message))
                        } else {
                            completion(.failure(dataResponse.message))
                        }
                    }catch {
                        completion(.failure(error.localizedDescription))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func setFavorite(id:Int,status:Bool,completion: @escaping (ApiResult<String?>) -> Void){
        let url = "\(BASE_URL)/model/favorite?lang=\(getLang())"
        let params = ["model_id" : id , "model_type" : "service","status": status ? "true" : "false" ] as [String : Any]
        Alamofire.request(url, method: .post, parameters: params,headers: getHeader())
            .responseString{(response) in
                print(response)
                switch response.result {
                case .success(_):
                    if let data = response.data {
                        do {
                            let dataResponse = try JSONDecoder().decode(PureResponse.self, from: data)
                            if dataResponse.status {
                                completion(.success(dataResponse.message))
                            }else {
                                completion(.failure(dataResponse.message))
                            }
                        }catch let e {
                            completion(.failure(e.localizedDescription))
                        }
                    }
                    break
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
        
    }
    
    func createRequest(serviceId : Int , date : String ,time :String ,completion: @escaping (ApiResult<String?>) -> Void) {
        
        let url = "\(BASE_URL)/request/create?lang=\(getLang())"
        let params = ["service_id" : serviceId , "date" : date , "time" :time ] as [String : Any]
        Alamofire.request(url, method: .post, parameters: params, headers: getHeader())
            .responseJSON { (response) in
                switch response.result {
                case .success:
                    let jsonData = response.data
                    
                    do{
                        let dataResponse = try JSONDecoder().decode(PureResponse.self, from: jsonData!)
                        if dataResponse.status {
                            completion(.success(dataResponse.message))
                        } else {
                            completion(.failure(dataResponse.message))
                        }
                    }catch {
                        completion(.failure(error.localizedDescription))
                    }
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    func getFavorite(completion: @escaping (ApiResult<[Favorite]?>) -> Void){
        let url = "\(BASE_URL)/model/favorite/list?lang=" + getLang()
        Alamofire.request(url, method: .get,headers: getHeader())
            .responseJSON{(response) in
                switch response.result {
                case .success(_):
                    if let data = response.data {
                        do {
                            let dataResponse = try JSONDecoder().decode(BaseResponse<[Favorite]>.self, from: data)
                            if dataResponse.status {
                                completion(.success(dataResponse.data))
                            }else {
                                completion(.failure(dataResponse.message))
                            }
                        }catch let e {
                            completion(.failure(e.localizedDescription))
                        }
                    }
                    break
                case let .failure(error):
                    completion(.failure(error.localizedDescription))
                }
        }
    }
    
    
    
    func editService(serviceId : Int , description: String,seats : String , subCategory : String , photos : [UIImage] ,price : String ,deleteImage : [Int] ,completion: @escaping (ApiResult<String>) -> Void){
        let url = "\(BASE_URL)/saloon/service/update/\(serviceId)?lang=\(getLang())"
//        var params = [String:Any]()
        var deletedImg = ""
        for item in deleteImage {
            deletedImg += "\(item),"
        }
        deletedImg.remove(at: deletedImg.index(before: deletedImg.endIndex))
        let params = ["description" : description , "seats" :seats ,"subCategoryId" : subCategory , "price" : price , "deleteImage[]" : deletedImg ] as [String : String]

        
        Alamofire.upload(multipartFormData: { multipartFormData in
            //Parameter for Upload files
            for photo in photos {
                let imageData = photo.jpegData(compressionQuality: 1.5)
                multipartFormData.append(imageData!, withName: "image[]", fileName: "\(Date().timeIntervalSince1970).jpeg", mimeType: "image/jpeg")
            }
            
            for (key, value) in params
            {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
            
        }, usingThreshold:UInt64.init(),
           to: url, //URL Here
            method: .post,
            headers: getHeader(),
            encodingCompletion: { (result) in
                switch result {
                case .success(let upload, _, _):
                    upload.uploadProgress(closure: { (progress) in
                    })
                    upload.responseString { response in
                        print(response)
                        switch response.result {
                        case .success(_):
                            if let data = response.data {
                                do {
                                    let dataResponse = try JSONDecoder().decode(BaseResponse<SaloonService>.self, from: data)
                                    print(dataResponse)
                                    if dataResponse.status {
                                        completion(.success(dataResponse.message))
                                    }else {
                                        completion(.failure(dataResponse.message))
                                    }
                                }catch let e {
                                    completion(.failure(e.localizedDescription))
                                }
                            }
                            break
                        case let .failure(error):
                            completion(.failure(error.localizedDescription))
                        }
                    }
                    break
                case .failure(let error):
                    completion(.failure(error.localizedDescription))
                    break
                }
        })
    }
}
