//
//  FavoritesTableViewCell.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/29/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

protocol IFavoritesTableViewCell {
    func click(position:Int)
}


class FavoritesTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var likeBtn: UIButton!
    
    var index = 0
    var delegate:IFavoritesTableViewCell?
    

    func setFavorite(favorites : Favorite)
    {
        timeLabel.text = favorites.time
        nameLabel.text = favorites.name
        descriptionLabel.text = favorites.description
    }
    @IBAction func likeAction(_ sender: Any) {
        delegate?.click(position: index)
    }
}
