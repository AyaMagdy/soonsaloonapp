//
//  Comment.swift
//  SoonSaloon
//
//  Created by alexlab on 5/10/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation

struct Comment : Codable{
    var rate = "0"
    var userName = ""
    var comment = ""
    var date = ""
    
    enum CodingKeys: String, CodingKey {
        case rate = "rate"
        case userName = "user_name"
        case comment = "comment"
        case date = "date"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.rate = try container.decodeIfPresent(String.self, forKey: .rate) ?? "0"
        self.userName = try container.decodeIfPresent(String.self, forKey: .userName) ?? ""
        self.comment = try container.decodeIfPresent(String.self, forKey: .comment) ?? ""
        self.date = try container.decodeIfPresent(String.self, forKey: .date) ?? ""
    }
}
