//
//  Saloon.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/30/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//


import Foundation



struct UserResponse: Codable {
    let user: User?
    let token :String?
}

struct User:Codable {
    var id = 0
    var name = ""
    var phone = ""
    var country = ""
    var city = ""
    var profileImage = ""
    var model:Model?

        enum CodingKeys: String, CodingKey {
            case id = "id"
            case name = "name"
            case phone = "phone"
            case country = "country"
            case city = "city"
            case profileImage = "profile_image"
            case model = "model"
        }
    init(from decoder: Decoder) throws
        {
            let container = try decoder.container(keyedBy: CodingKeys.self)
    
            self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
            self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
            self.phone = try container.decodeIfPresent(String.self, forKey: .phone) ?? ""
            self.country = try container.decodeIfPresent(String.self, forKey: .country) ?? ""
            self.city = try container.decodeIfPresent(String.self, forKey: .city) ?? ""
            self.profileImage = try container.decodeIfPresent(String.self, forKey: .profileImage) ?? ""
            self.model = try container.decodeIfPresent(Model.self, forKey: .model) ?? Model(from: decoder)
        }
    
    init(name:String , phone :String ,country:String , city :String ,profileImage :String , latitude:Double ,longitude:Double , openingTimeFrom:String , openingTimeTo:String) {
        self.name = name
        self.phone = phone
        self.country = country
        self.city = city
        self.profileImage = profileImage
        self.name = name
        self.model?.latitude = latitude
        
    }
    init() {
        
    }
}


struct Model:Codable {
    var latitude = 0.0
    var longitude = 0.0
    var openingTimeFrom = ""
    var openingTimeTo = ""
    var openingDayFrom = 0
    var openingDayTo = 0
    var gender = 0

        enum CodingKeys: String, CodingKey {
            case latitude = "latitude"
            case longitude = "longitude"
            case openingTimeFrom = "opening_time_from"
            case openingTimeTo = "opening_time_to"
            case openingDayFrom = "opening_day_from"
            case openingDayTo = "opening_day_to"
            case gender = "gender"
        }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.latitude = try container.decodeIfPresent(Double.self, forKey: .latitude) ?? 0.0
        self.longitude = try container.decodeIfPresent(Double.self, forKey: .longitude) ?? 0.0
        self.openingTimeFrom = try container.decodeIfPresent(String.self, forKey: .openingTimeFrom) ?? ""
        self.openingTimeTo = try container.decodeIfPresent(String.self, forKey: .openingTimeTo) ?? ""
        self.openingDayFrom = try container.decodeIfPresent(Int.self, forKey: .openingDayFrom) ?? 0
        self.openingDayTo = try container.decodeIfPresent(Int.self, forKey: .openingDayTo) ?? 0
        self.gender = try container.decodeIfPresent(Int.self, forKey: .gender) ?? 0
    }
    
    init() {
    }
}
