//
//  Service.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/5/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation


struct SaloonService:Codable {
    var id = 0
    var seats = ""
    var subCategory = SubCategory
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case seats = "seats"
        case subCategory = "subCategory"

    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        self.seats = try container.decodeIfPresent(String.self, forKey: .seats) ?? ""
        self.subCategory = try container.decodeIfPresent(SubCategory.self, forKey: .model) ?? SubCategory(from: decoder)
    }
}


struct SubCategory:Codable {
    var name = ""
    var image = ""
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case image = "image"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        self.image = try container.decodeIfPresent(String.self, forKey: .image) ?? ""
    }
}

