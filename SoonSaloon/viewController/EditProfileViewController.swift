//
//  ProfileViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/4/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class EditProfileViewController: BaseViewController,UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate ,UINavigationControllerDelegate,LocationProtocol {
    
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var dayToTextField: UITextField!
    @IBOutlet weak var dayFromTextField: UITextField!
    @IBOutlet weak var timeToTextField: UITextField!
    @IBOutlet weak var timeFromTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    
    @IBOutlet var locationTextFiled: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var userImageView: UIImageView!
    
    var allDays = Constant.Days
    var countryList = [City]()
    
    var cityList = [City]()
    
    var picker = UIPickerView()
    let datePickerView = UIDatePicker()
    let timePickerView = UIDatePicker()
    
    var photoBase64 = ""
    var countryIdSelected = 0
    var cityIdSelected = 0
    var lat = 0.0
    var lng = 0.0
    
    var imagePicker = UIImagePickerController()
    @IBOutlet var loading: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.dataSource = self
        picker.delegate = self
        countryTextField.inputView = picker
        cityTextField.inputView = picker
        
        dayFromTextField.inputView = picker
        dayToTextField.inputView = picker
        
        showDateTimePicker()
        
        imagePicker.delegate = self
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.selectImage))
        self.userImageView.isUserInteractionEnabled = true
        self.userImageView.addGestureRecognizer(gesture)
        
        
        if let user = UserCache.get() {
            countryIdSelected = Int(user.country) ?? 0
            cityIdSelected = Int(user.city) ?? 0
            getCountries()
        }
    }
    @IBAction func selectedCountry(_ sender: Any) {
        getCities(countryId: countryIdSelected,setDataAfterLoad: false)
        cityIdSelected = 0
    }
    
    @IBAction func selectCountryAction(_ sender: Any) {
        getCities(countryId: countryIdSelected,setDataAfterLoad: false)
        cityIdSelected = 0
    }
    
    @IBAction func selectLocation(_ sender: Any) {
        let showItemStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let showItemNavController = showItemStoryboard.instantiateViewController(withIdentifier: "Map") as! MapViewController
        showItemNavController.locationProtocol = self
        present(showItemNavController, animated: true)
    }
    
    
    func completePin(latitude: Double, longitude: Double) {
        lat = latitude
        lng = longitude
        locationTextFiled.text = "\(latitude) , \(longitude)"
        Utlities.getAddressFromLatLon(lat: latitude, lon: longitude){result in
            switch result {
            case let .success(address):
                self.locationTextFiled.text = address
            case .failure(_):
                print("")
            }
        }
    }
    
    
    func setUserData(){
        if let user = UserCache.get() , let model = user.model {
            nameTextField.text = user.name
            Utlities.setImage(imageView:userImageView,url:user.profileImage)
            timeFromTextField.text = model.openingTimeFrom
            timeToTextField.text = model.openingTimeTo
            dayFromTextField.text = allDays[(model.openingDayFrom)-1]
            dayToTextField.text =  allDays[(model.openingDayTo)-1]
            phoneTextField.text = user.phone
            
            
            for item in countryList {
                if item.id == countryIdSelected {
                    countryTextField.text = item.name
                    break
                }
            }
            
            for item in cityList {
                if item.id == cityIdSelected {
                    cityTextField.text = item.name
                    break
                }
            }
            
            if model.latitude > 0 && model.longitude > 0 {
                Utlities.getAddressFromLatLon(lat: model.latitude, lon: model.longitude) {result in
                    switch result {
                    case let .success(address):
                        self.locationTextFiled.text = address
                    case .failure(_):
                        print("")
                    }
                }
            }
            
        }
    }
    func showDateTimePicker(){
        timePickerView.datePickerMode = UIDatePicker.Mode.time
        timeFromTextField.inputView = timePickerView
        timeToTextField.inputView = timePickerView
        timePickerView.addTarget(self, action: #selector(timePickerFromValueChanged), for: UIControl.Event.valueChanged)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if countryTextField.isFirstResponder {
            return countryList.count
        }else if cityTextField.isFirstResponder {
            return cityList.count
        }else if dayFromTextField.isFirstResponder {
            return allDays.count
        }else if dayToTextField.isFirstResponder {
            return allDays.count
        }else {
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if countryTextField.isFirstResponder {
            return countryList[row].name
        }else if cityTextField.isFirstResponder {
            return cityList[row].name
        }else if dayFromTextField.isFirstResponder {
            return allDays[row]
        }else if dayToTextField.isFirstResponder {
            return allDays[row]
        }else {
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if countryTextField.isFirstResponder {
            countryIdSelected = countryList[row].id ?? 0
            countryTextField.text = countryList[row].name
        }else if cityTextField.isFirstResponder {
            cityTextField.text = cityList[row].name
            cityIdSelected = cityList[row].id ?? 0
        }else if dayFromTextField.isFirstResponder {
            return dayFromTextField.text = allDays[row]
        }else if dayToTextField.isFirstResponder {
            return dayToTextField.text = allDays[row]
        }
    }
    
    func getCities(countryId : Int,setDataAfterLoad:Bool)
    {
        loading.startAnimating()
        RequestAPI.shared.allCities(CountryId: countryId)
        { result in
            switch result {
            case let .success(cities):
                self.loading.stopAnimating()
                self.cityList = cities!
                if setDataAfterLoad {
                    self.setUserData()
                }
                break
            case let .failure(error):
                self.loading.stopAnimating()
                Utlities.showAlert(vc: self, msg:error)
                break
            }
        }
    }
    
    @objc func timePickerFromValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        
        if timeFromTextField.isFirstResponder {
            timeFromTextField.text = dateFormatter.string(from: sender.date)
        }else if timeToTextField.isFirstResponder {
            timeToTextField.text = dateFormatter.string(from: sender.date)
        }
    }
    
    @objc func selectImage() {
        Utlities.selectImageFromGallery(vc: self, imagePicker: imagePicker)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:[UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            userImageView.image = image
        }
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func saveAction(_ sender: Any) {
        
        if  nameTextField.text == "" || cityTextField.text == "" || countryTextField.text == "" || timeFromTextField.text == "" || timeToTextField.text == "" || dayFromTextField.text == "" || dayToTextField.text == "" || phoneTextField.text == "" {
            
            Utlities.showAlert(vc: self, msg: Constant.fillAllForm)
        }
        else{
            updateProfile()
        }
    }
    
    
    func getCountries()
    {
        loading.startAnimating()
        RequestAPI.shared.getCountries()
            { result in
                switch result {
                case let .success(cities):
                    self.loading.stopAnimating()
                    self.countryList = cities!
                    self.getCities(countryId: self.countryIdSelected,setDataAfterLoad: true)
                    break
                case let .failure(error):
                    self.loading.stopAnimating()
                    Utlities.showAlert(vc: self, msg:error)
                    break
                }
        }
    }
    
    func updateProfile() {
        showDialog(vc: self)
        RequestAPI.shared.updateUser(name: nameTextField.text!,phone : phoneTextField.text! , city : cityIdSelected , country : countryIdSelected , dayFrom : allDays.index(of: dayFromTextField.text!)!+1, dayTo : allDays.index(of: dayToTextField.text!)!+1, timeFrom :  timeFromTextField.text! , timeto : timeToTextField.text! , photo : userImageView.image!,latitude: lat,longitude: lng)
        { result in
            switch result {
            case let .success(userData):
                UserCache.save(userData)
                self.dismissDialog()
                Utlities.showAlert(vc: self,title:"Success", msg: Constant.profileUpdated)
                break
            case let .failure(error):
                self.dismissDialog()
                Utlities.showAlert(vc: self, msg: error)
                break
            }
        }
    }
}
