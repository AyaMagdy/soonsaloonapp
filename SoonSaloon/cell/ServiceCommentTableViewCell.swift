//
//  ServiceCommentTableViewCell.swift
//  SoonSaloon
//
//  Created by alexlab on 5/10/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit
import Cosmos

class ServiceCommentTableViewCell: UITableViewCell {

    @IBOutlet weak var ratingBar: CosmosView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setData(comment:Comment?) {
        if let item = comment {
            ratingBar.rating = Double(item.rate) ?? 0
            ratingBar.text = item.userName
            timeLabel.text = item.date
            commentLabel.text = item.comment
        }
    }

}
