//
//  UserCurrentRequestsViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/13/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class UserCurrentRequestsViewController: BaseViewController, UITableViewDelegate , UITableViewDataSource , CellProtocolIndex ,EditProtocol {
    
    
    
    @IBOutlet weak var currentRequestTableView: UITableView!
    
    var allRequests : [Request] = []
    var refreshControl = UIRefreshControl()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Current"
        
        refreshControl.addTarget(self, action: #selector(getHistory), for: .valueChanged)
        currentRequestTableView.addSubview(refreshControl)
        
        getHistory()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allRequests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "currentCell" ) as! UserCurrentRequestsTableViewCell
        let request = allRequests[indexPath.row]
        cell.setRequest(requests : request)
        cell.index = indexPath.row
        cell.cellProtocol = self
        return cell
    }
    
    @objc func getHistory()
    {
        refreshControl.beginRefreshing()
        RequestAPI.shared.getRequestCurrent()
            { result in
                switch result {
                case let .success(requests):
                    self.refreshControl.endRefreshing()
                    self.allRequests = requests!
                    self.currentRequestTableView.reloadData(
                        with: .simple(duration: 0.75, direction: .rotation3D(type: .ironMan),
                                      constantDelay: 0))
                    break
                case let .failure(error):
                    self.refreshControl.endRefreshing()
                    DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1, execute: {
                        Utlities.showAlert(vc: self, msg:error)
                    })
                    break
                }
        }
    }
    
    
    
    func click(position: Int, type: actionType) {
        let refreshAlert = UIAlertController(title: Constant.areYouSure, message: "", preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: Constant.yes, style: .default, handler: { (action: UIAlertAction!) in
            if (type == .edit)
            {
                let popOverVC = UIStoryboard(name: "SaloonRequests", bundle: nil).instantiateViewController(withIdentifier: "editRequest") as! EditRequestViewController
                popOverVC.request = self.allRequests[position]
                popOverVC.index = position
                self.addChild(popOverVC)
                popOverVC.view.frame = self.view.frame
                self.view.addSubview(popOverVC.view)
                popOverVC.didMove(toParent: self)
                popOverVC.serviceProtocol = self
            }
            else if type == .cancel {
                self.popUp(title : Constant.cancelTitle, message: Constant.cancelMessage, type: .cancel , position : position)
            }
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: Constant.no, style: .cancel, handler: { (action: UIAlertAction!) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        
    }
    
    func popUp(title : String , message: String , type : actionType , position : Int ){
        let id = self.allRequests[position].id
        
        let alert = UIAlertController(title: title, message: message , preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: Constant.alertAccept, style: UIAlertAction.Style.default, handler: { (alert: UIAlertAction!) -> Void in
            if (type == .cancel){
                self.CancelRequest(requestId : id , position : position)
            }
        }))
        
        alert.addAction(UIAlertAction(title: Constant.alertReject, style: UIAlertAction.Style.default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    
    func CancelRequest(requestId : Int , position : Int)
    {
        showDialog(vc: self)
        RequestAPI.shared.cancelRequest(requestId: requestId)
        { result in
            switch result {
            case .success(_):
                self.dismissDialog()
                self.allRequests.remove(at: position)
                self.currentRequestTableView.reloadData(
                    with: .simple(duration: 0.75, direction: .rotation3D(type: .ironMan),
                                  constantDelay: 0))
                break
            case let .failure(error):
                self.dismissDialog()
                DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1, execute: {
                    Utlities.showAlert(vc: self, msg:error)
                })
                break
            }
        }
    }
    
    
    func completeEdit(requestObj: Request, position: Int) {
        self.allRequests[position] = requestObj
        self.currentRequestTableView.reloadData(
            with: .simple(duration: 0.75, direction: .rotation3D(type: .ironMan),
                          constantDelay: 0))
    }
    
}
