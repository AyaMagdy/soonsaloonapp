//
//  UserMenuViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/28/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class UserMenuViewController: BaseViewController {
    
    @IBOutlet var favoriteButton: UIButton!
    @IBOutlet var requestsButton: UIButton!
    
 
    
    @IBOutlet var profileLabel: UIButton!
    @IBOutlet var loginUserLabel: UIButton!
    @IBOutlet var notificationButton: UIButton!
    
    @IBOutlet var loginSaloonLabel: UIButton!
    @IBOutlet weak var userImageView: UIImageView!
    
    @IBOutlet var userNameLabel: UILabel!
    @IBOutlet var userPhoneLabel: UILabel!
    @IBOutlet var logoutButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        if let user = UserCache.get(){

            notificationButton.isHidden = false
            favoriteButton.isHidden = false
            requestsButton.isHidden = false
            profileLabel.isHidden = false
            Utlities.setImage(imageView: userImageView, url: user.profileImage)
            userNameLabel.text = user.name
            userPhoneLabel.text = user.phone
            loginSaloonLabel.isHidden = true
            loginUserLabel.isHidden = true
            logoutButton.isHidden = false
        }else {
            userImageView?.setImage(string: "Soon Saloon", color: UIColor.white , circular: true)
            userNameLabel.text = ""
            userPhoneLabel.text = ""
            notificationButton.isHidden = true
            favoriteButton.isHidden = true
            requestsButton.isHidden = true
            profileLabel.isHidden = true
            loginSaloonLabel.isHidden = false
            loginUserLabel.isHidden = false
            logoutButton.isHidden = true
        }
    }
    
    @IBAction func logout(_ sender: Any) {
        let refreshAlert = UIAlertController(title: Constant.logout, message: Constant.areYouSure, preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: Constant.yes, style: .default, handler: { (action: UIAlertAction!) in
            UserTypeCache.remove()
            UserCache.remove()
            TokenCache.remove()
            
            // request log out without loading or show error msg
            // open splash screen and clear stack
            self.logout()
            self.logoutfromStack()

        }))
        
        refreshAlert.addAction(UIAlertAction(title: Constant.no, style: .cancel, handler: { (action: UIAlertAction!) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    
    func logout()
    {
        RequestAPI.shared.logout()
            { result in
                switch result {
                case .success(_):
                    break
                case let .failure(error):
                    Utlities.showAlert(vc: self, msg:error)
                    break
                }
        }
        
    }
    
    func logoutfromStack()
    {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        // Should remove all subsequent view controllers from memory.
        appDelegate.window?.rootViewController!.dismiss(animated: true, completion: nil)
        
        // Set the root view controller to a new instance of the sign in view controller.
        appDelegate.window?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SplashViewController")
    }
    

    @IBAction func closeMenuAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}
