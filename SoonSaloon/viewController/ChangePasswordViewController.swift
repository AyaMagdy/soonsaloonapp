//
//  ChangePasswordViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/8/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class ChangePasswordViewController: BaseViewController {

    
    @IBOutlet weak var oldPassword: UITextField!
    
    @IBOutlet weak var newPassword: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func changePassword() {
        showDialog(vc: self)
        RequestAPI.shared.changePassword(oldPassword: oldPassword.text!,newPassword : newPassword.text!)
        { result in
            switch result {
            case .success(_):
                self.dismissDialog()
                Utlities.showAlert(vc: self,title:Constant.success, msg: Constant.updated)
                break
            case let .failure(error):
                self.dismissDialog()
                Utlities.showAlert(vc: self, msg: error)
                break
            }
        }
    }
    

    @IBAction func saveAction(_ sender: Any) {
        if oldPassword.text == "" || newPassword.text == "" || confirmPassword.text == "" {
            Utlities.showAlert(vc: self, msg: Constant.fillAllForm)
        }
        else {
            if newPassword.text != confirmPassword.text {
                Utlities.showAlert(vc: self, msg: Constant.checkConfirmPassword)
            }
            else
            {
                changePassword()
            }
        }
    }
}
