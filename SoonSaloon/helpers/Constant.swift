//
//  Constant.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/2/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation

enum UserType {
    case saloon
    case customer
}

class Constant {
    static let nextToSecondRegisterPage = "nextToSecondRegisterPage";
    static let nextToThirdRegisterVC = "nextToThirdRegisterVC"
    static let usernextToSecondRegisterPage = "userReqisterNext"
    
    
    static let loginError = "Invalid Email or Password"
//    static let fillAllForm = "Please Fill All Form Input"
    static let fillAllForm = NSLocalizedString("Please Fill All Form Input", comment: "")
    static let fillRate = "Please Enter Rate Value"
    static let passwordMatch = "password and confirm password not matched"
    
    static let dateFormate = "HH:mm"
    
    static let SaloonSideMenuStoryBoardIdentifier = "SaloonSideMenu"
    static let SaloonHomeIdentifier = "SaloonHome"
    static let UserSideMenuIdentifier = "UserSideMenu"
    static let userHomeIdentifier = "userHome"
    static let UserAuthStoryBoardIdentifier = "UserAuth"
    
    
    
    
    static let profileUpdated = "Profile Updated Successfully"
    static let serviceAdded = "Service Added  Successfully"
    static let changeStatusTitle = "Change Request Status"
    static let changeStatusMessage = "Change Status ?"
    
    static let cancelTitle = "Cancel Request"
    static let cancelMessage = "Cancel This Request ?"
    
    static let alertAccept = "Yes"
    static let alertReject = "No"
    static let checkConfirmPassword = "Password And Confirm Password Not The Same"
    static let MyProfile = "My Profile"
    static let seats = "Seats"
    static let to = "To"
    static let Days = ["day1","day2","day3","day4","day5","day6","day7"]
    
    static let deleteServicePopTitle = "Delete Service"
    static let deleteServicePopMessage = "Are You Sure To Delete This Service ?"
    static let areYouSure = "are you sure"
    static let logout = "Logout"
    static let delete = "Delete"
    static let yes = "yes"
    static let no = "no"
    static let ok = "OK"
    static let updated = "Updated"
    static let success = "Success"
    static let viewAll = "VIEW ALL"
    static let no_data = "Not Found Data"
    static let mapTitle = "Select Location"
    static let mapMessage = "Please Select location"
    static let workingTime = "Working Time"
    static let workingDay = "Working Day"
    static let saloonName = "Saloon Name"
    
}


class Days {
    var id : Int
    var nameAr : String
    var nameEN : String
    
    init(id : Int ,nameAr : String ,nameEN : String ) {
        self.id = id
        self.nameAr = nameAr
        self.nameEN = nameEN
    }
}
