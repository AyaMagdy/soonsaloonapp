//
//  SaloonSubCategoryTableViewCell.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/29/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class SaloonSubCategoryTableViewCell: UITableViewCell {

    @IBOutlet weak var saloonLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var subCategoryImageView: UIImageView!
    
   
    func setData(category : SubCategories?)
    {
        if let item = category{
            saloonLabel.text =  Utlities.getCurrentValueForLang(str: item.numberOfSaloon)
            Utlities.setImage(imageView: subCategoryImageView, url: item.image)
            nameLabel.text = item.name
            descriptionLabel.text = item.description
        }
    }
}
