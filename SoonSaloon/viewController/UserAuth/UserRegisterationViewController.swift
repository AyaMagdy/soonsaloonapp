//
//  UserRegisterationViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/25/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class UserRegisterationViewController: BaseViewController ,UIPickerViewDelegate,UIPickerViewDataSource{

    var countryList = [City]()
    
    var cityList = [City]()
    
    var picker = UIPickerView()
    
    @IBOutlet weak var gender: UISegmentedControl!
    @IBOutlet weak var countryTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    
    var countryIdSelected = 0
    var cityIdSelected = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.dataSource = self
        picker.delegate = self
        

        countryTextField.inputView = picker
        cityTextField.inputView = picker
        
        getCountries()

    }
    
    @IBAction func countrySelectedAction(_ sender: Any) {
        getCities(countryId: countryIdSelected)
        cityIdSelected = 0
    }
    
    func getCountries()
    {
        showDialog(vc: self)
        RequestAPI.shared.getCountries()
        { result in
            switch result {
            case let .success(cities):
                self.dismissDialog()
                self.countryList = cities!
                break
            case let .failure(error):
                self.dismissDialog()
                Utlities.showAlert(vc: self, msg:error)
                break
            }
        }
    }

    
    func getCities(countryId : Int)
    {
        showDialog(vc: self)
        RequestAPI.shared.allCities(CountryId: countryId)
        { result in
            switch result {
            case let .success(cities):
                self.dismissDialog()
                self.cityList = cities!
                self.cityTextField.text = self.cityList.first?.name
                break
            case let .failure(error):
                self.dismissDialog()
                Utlities.showAlert(vc: self, msg:error)
                break
            }
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if countryTextField.isFirstResponder {
            return countryList.count
        }else if cityTextField.isFirstResponder {
            return cityList.count
        }else {
            return 0
        }
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if countryTextField.isFirstResponder {
            return countryList[row].name
        }else if cityTextField.isFirstResponder {
            return cityList[row].name
        }else {
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if countryTextField.isFirstResponder {
            countryTextField.text = countryList[row].name
            countryIdSelected = countryList[row].id ?? 0
        }else if cityTextField.isFirstResponder {
            cityTextField.text = cityList[row].name
            cityIdSelected = cityList[row].id ?? 0
        }
    }
 
    @IBAction func nextAction(_ sender: Any) {
        if cityTextField.text == "" || countryTextField.text == ""  {
            Utlities.showAlert(vc: self, msg: Constant.fillAllForm)
        }
        else{
            performSegue(withIdentifier: Constant.usernextToSecondRegisterPage, sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let gender = self.gender.titleForSegment(at: self.gender.selectedSegmentIndex)
        
        if countryIdSelected == 0 || cityIdSelected == 0 {
            Utlities.showAlert(vc: self, msg: Constant.fillAllForm)
        }else if segue.identifier == Constant.usernextToSecondRegisterPage {
            let destinationVC = segue.destination as! UserRegisterationSecodViewController
            var userObj = User()
            userObj.city = "\(cityIdSelected)"
            userObj.country = "\(countryIdSelected)"
            userObj.model = Model()
            userObj.model?.gender = self.gender.selectedSegmentIndex
            destinationVC.user = userObj
            
        }
    }
    
}

