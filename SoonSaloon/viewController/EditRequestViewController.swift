//
//  EditRequestViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/10/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

protocol EditProtocol {
    func completeEdit(requestObj: Request ,position : Int )
}

class EditRequestViewController: BaseViewController,UIPickerViewDelegate,UIPickerViewDataSource {

    @IBOutlet weak var nameTextField: UILabel!
    @IBOutlet weak var phoneTextField: UILabel!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var timeTextField: UITextField!
    
    var picker = UIPickerView()
    let datePickerView = UIDatePicker()
    let timePickerView = UIDatePicker()
    
    var request : Request?
    var index: Int?
    var serviceProtocol : EditProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        
        picker.dataSource = self
        picker.delegate = self
        dateTextField.inputView = picker
        showDateTimePicker()
        setRequest()
    }
    
    
    func setRequest(){
        if UserTypeCache.get() == .saloon {
            nameTextField.text = request?.customerName
            phoneTextField.text = request?.customerPhone
        }
        else
        {
            nameTextField.text = request?.saloonName
            phoneTextField.text = request?.saloonPhone
        }
        dateTextField.text = convertDateFormater((request?.date)!, from: "yyyy-MM-dd" , to: "dd/MM/yyyy")
        timeTextField.text = convertTimeFormater((request?.time)!)
    }
    
    func showDateTimePicker(){
        timePickerView.datePickerMode = UIDatePicker.Mode.time
        timeTextField.inputView = timePickerView
        timePickerView.addTarget(self, action: #selector(timePickerFromValueChanged), for: UIControl.Event.valueChanged)
        datePickerView.datePickerMode = UIDatePicker.Mode.date
        datePickerView.minimumDate = Date()
        dateTextField.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(timePickerFromValueChanged), for: UIControl.Event.valueChanged)
    }
    
    
    
    @objc func timePickerFromValueChanged(sender:UIDatePicker) {
        let timeFormatter = DateFormatter()
        let dateFormatter = DateFormatter()
        
        timeFormatter.dateFormat = "HH:mm"
        dateFormatter.dateFormat = "dd/MM/yyyy"
        if timeTextField.isFirstResponder {
            timeTextField.text = timeFormatter.string(from: sender.date)
        }else if dateTextField.isFirstResponder {
            dateTextField.text = dateFormatter.string(from: sender.date)
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 0
    }
    
    func convertDateFormater(_ date: String , from : String , to : String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = from
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = to
        return  dateFormatter.string(from: date!)
    }
    
    func convertTimeFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "HH:mm"
        return  dateFormatter.string(from: date!)
    }

    @IBAction func closeAction(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    
    @IBAction func updateRequestAction(_ sender: Any) {
        if dateTextField.text == "" || timeTextField.text == ""  {
            Utlities.showAlert(vc: self, msg: Constant.fillAllForm)
        }
        else{
             let date = convertDateFormater((dateTextField.text)!, from: "dd/MM/yyyy" , to: "yyyy-MM-dd" )
            editRequest(requestId : (request?.id)! , date : date , time : timeTextField.text!)
        }
    }
    
    func editRequest(requestId : Int , date : String , time : String)
    {print(requestId)
        showDialog(vc: self)
        RequestAPI.shared.editRequest(requestId: requestId, date: date ,time: time)
        { result in
            switch result {
            case let .success(request):
                self.dismissDialog()
                self.request = request
                self.serviceProtocol?.completeEdit(requestObj: request!, position: self.index!)
                self.view.removeFromSuperview()
                break
            case let .failure(error):
                self.dismissDialog()
                DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1, execute: {
                    Utlities.showAlert(vc: self, msg:error)
                })
                break
            }
        }
    }
}
