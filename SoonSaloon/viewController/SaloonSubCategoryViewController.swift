//
//  SaloonSubCategoryViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/29/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//




import UIKit
import TableViewReloadAnimation

class SaloonSubCategoryViewController: UIViewController ,UITableViewDataSource ,UITableViewDelegate {
   
    var refreshControl = UIRefreshControl()

    @IBOutlet weak var subCategoryTableView: UITableView!
    var list : [SubCategories] = []
    var categoryId = 0
    var selectedRow = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.title = title
        refreshControl.addTarget(self, action: #selector(getSubCategories), for: .valueChanged)
        subCategoryTableView.addSubview(refreshControl)
        getSubCategories()
    }
    

   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "subCategoryCell" ) as! SaloonSubCategoryTableViewCell
        let category = list[indexPath.row]
        cell.setData(category : category)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRow = indexPath.row
        performSegue(withIdentifier: "show_service_in_saloon", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "show_service_in_saloon") {
            // pass data to next view
            let destinationVC = segue.destination as! SaloonsViewController
            destinationVC.subCategoryId = list[selectedRow].id
            destinationVC.title = list[selectedRow].name
        }
    }
    
    @objc func getSubCategories(){
        refreshControl.beginRefreshing()
        RequestAPI.shared.getSubCategories(CategoryId:categoryId)
            { result in
                switch result {
                case let .success(list):
                    self.list = list ?? []
                    self.refreshControl.endRefreshing()
                    self.subCategoryTableView.reloadData()
                    self.subCategoryTableView.reloadData(
                        with: .spring(duration: 0.45, damping: 0.65, velocity: 1, direction: .right(useCellsFrame: false),
                                      constantDelay: 0))
                    break
                case let .failure(error):
                    self.refreshControl.endRefreshing()
                    Utlities.showAlert(vc: self, msg:error)
                    break
                }
        }
    }
}
