//
//  CategoryHeaderTableViewCell.swift
//  SoonSaloon
//
//  Created by alexlab on 5/14/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation
import UIKit


class CategoryHeaderTableViewCell: UITableViewCell {
    
    
    @IBOutlet var nameLabelTxt: UILabel!
    
    func setData(category : CategoryAndSubCategory?)
    {
        if let item = category{
            nameLabelTxt.text = item.name
        }
    }
    
}
