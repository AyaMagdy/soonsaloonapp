//
//  SaloonsTableViewCell.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/29/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//


protocol CellProtocol {
    func click(position:Int,rate :Int )
}

import UIKit
import Cosmos

class SaloonsTableViewCell: UITableViewCell {

    @IBOutlet weak var saloonImageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    
    
    func setData(service : SaloonService?){
        if let item = service {
            priceLabel.text = item.price
            nameLabel.text = item.saloonName
            rateView.rating = Double(item.rate)
            descriptionLabel.text = item.description
            Utlities.setImage(imageView: saloonImageView, url: item.saloonImage)
        }
    }
    
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//        rateView.didFinishTouchingCosmos = { [weak self] rating in
//            self!.delegate?.click(position: self!.index, rate: Int(rating))
//        }
//    }
}
