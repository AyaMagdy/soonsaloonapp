//
//  SaloonsViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/29/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//



import UIKit
import Cosmos

class SaloonsViewController: UIViewController,UITableViewDataSource ,UITableViewDelegate {

    @IBOutlet weak var SaloonsTableView: UITableView!
    var list : [SaloonService] = []
    var refreshControl = UIRefreshControl()

    var subCategoryId = 0
    var selectedRow = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshControl.addTarget(self, action: #selector(getServices), for: .valueChanged)
        SaloonsTableView.addSubview(refreshControl)
        getServices()

    }
    
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return list.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "saloonCell" ) as! SaloonsTableViewCell
        let service = list[indexPath.row]
        cell.setData(service: service)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRow = indexPath.row
        performSegue(withIdentifier: "service_details", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "service_details") {
            // pass data to next view
            let destinationVC = segue.destination as! UserServiceDetailsViewController
            destinationVC.serviceId = list[selectedRow].id
            destinationVC.title = list[selectedRow].saloonName
        }
    }
    
    
    @objc func getServices(){
        refreshControl.beginRefreshing()
        RequestAPI.shared.getServices(SubCategoryId:subCategoryId)
        { result in
            switch result {
            case let .success(list):
                self.list = list ?? []
                self.refreshControl.endRefreshing()
                self.SaloonsTableView.reloadData(
                    with: .spring(duration: 0.45, damping: 0.65, velocity: 1, direction: .right(useCellsFrame: false),
                                  constantDelay: 0))
                break
            case let .failure(error):
                self.refreshControl.endRefreshing()
                Utlities.showAlert(vc: self, msg:error)
                break
            }
        }
    }
}
