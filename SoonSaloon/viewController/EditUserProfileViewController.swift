//
//  EditUserProfileViewController.swift
//  SoonSaloon
//
//  Created by alexlab on 5/18/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class EditUserProfileViewController:BaseViewController,UIPickerViewDelegate,UIPickerViewDataSource,UIImagePickerControllerDelegate ,UINavigationControllerDelegate {
    
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    
    @IBOutlet weak var cityTextField: UITextField!
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var userImageView: UIImageView!
    
    var allDays = Constant.Days
    var countryList = [City]()
    
    @IBOutlet var loading: UIActivityIndicatorView!
    var cityList = [City]()
    
    var picker = UIPickerView()
    
    var photoBase64 = ""
    var countryIdSelected = 0
    var cityIdSelected = 0
    
    var imagePicker = UIImagePickerController()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.dataSource = self
        picker.delegate = self
        countryTextField.inputView = picker
        cityTextField.inputView = picker
        
        
        imagePicker.delegate = self
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.selectImage))
        self.userImageView.isUserInteractionEnabled = true
        self.userImageView.addGestureRecognizer(gesture)
        
        if let user = UserCache.get() {
            countryIdSelected = Int(user.country) ?? 0
            cityIdSelected = Int(user.city) ?? 0
            getCountries()
        }
    }
    
    @IBAction func selectCountryAction(_ sender: Any) {
        getCities(countryId: countryIdSelected,setDataAfterLoad: false)
        cityIdSelected = 0
    }
    
    func setUserData(){
        if let user = UserCache.get() {
            nameTextField.text = user.name
            Utlities.setImage(imageView:userImageView,url:user.profileImage)
            phoneTextField.text = user.phone
            
            
            for item in countryList {
                if item.id == countryIdSelected {
                    countryTextField.text = item.name
                    break
                }
            }
            
            for item in cityList {
                if item.id == cityIdSelected {
                    cityTextField.text = item.name
                    break
                }
            }
        }
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if countryTextField.isFirstResponder {
            return countryList.count
        }else if cityTextField.isFirstResponder {
            return cityList.count
        }else {
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if countryTextField.isFirstResponder {
            return countryList[row].name
        }else if cityTextField.isFirstResponder {
            return cityList[row].name
        }else {
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if countryTextField.isFirstResponder {
            countryIdSelected = countryList[row].id ?? 0
            countryTextField.text = countryList[row].name
        }else if cityTextField.isFirstResponder {
            cityTextField.text = cityList[row].name
            cityIdSelected = cityList[row].id ?? 0
        }
    }
    
    func getCities(countryId : Int,setDataAfterLoad:Bool)
    {
        loading.startAnimating()
        RequestAPI.shared.allCities(CountryId: countryId)
        { result in
            switch result {
            case let .success(cities):
                self.loading.stopAnimating()
                self.cityList = cities!
                if setDataAfterLoad {
                    self.setUserData()
                }
                break
            case let .failure(error):
                self.loading.stopAnimating()
                Utlities.showAlert(vc: self, msg:error)
                break
            }
        }
    }
    
    
    @objc func selectImage() {
        Utlities.selectImageFromGallery(vc: self, imagePicker: imagePicker)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:[UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            userImageView.image = image
        }
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func saveAction(_ sender: Any) {
        
        if  nameTextField.text == "" || cityTextField.text == "" || countryTextField.text == "" || phoneTextField.text == "" {
            
            Utlities.showAlert(vc: self, msg: Constant.fillAllForm)
        }
        else{
            updateProfile()
        }
    }
    
    func getCountries()
    {
        loading.startAnimating()
        RequestAPI.shared.getCountries()
            { result in
                switch result {
                case let .success(cities):
                    self.loading.stopAnimating()
                    self.countryList = cities!
                    self.getCities(countryId: self.countryIdSelected,setDataAfterLoad: true)
                    break
                case let .failure(error):
                    self.loading.stopAnimating()
                    Utlities.showAlert(vc: self, msg:error)
                    break
                }
        }
    }
    
    
    func updateProfile() {
        showDialog(vc: self)
        RequestAPI.shared.updateUser(name: nameTextField.text!,phone : phoneTextField.text! , city : cityIdSelected , country : countryIdSelected , photo : userImageView.image!)
        { result in
            switch result {
            case let .success(userData):
                UserCache.save(userData)
                self.dismissDialog()
                Utlities.showAlert(vc: self,title:"Success", msg: Constant.profileUpdated)
                break
            case let .failure(error):
                self.dismissDialog()
                Utlities.showAlert(vc: self, msg: error)
                break
            }
        }
    }
}
