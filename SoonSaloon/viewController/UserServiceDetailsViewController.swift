//
//  UserServiceDetailsViewController.swift
//  SoonSaloon
//
//  Created by alexlab on 5/10/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit
import Cosmos
import MapKit

class UserServiceDetailsViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var sliderImageView: UIImageView!
    @IBOutlet weak var sliderStackView: UIStackView!
    @IBOutlet weak var firstImageInSlider: UIImageView!
    @IBOutlet weak var serviceRatingBar: CosmosView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var seatsLabel: UILabel!
    @IBOutlet weak var saloonNameLabel: UILabel!
    
    var serviceId = 0
    var commentList = [Comment]()
    var service:SaloonService?
    
    @IBOutlet weak var tableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getServiceData()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell" ) as! ServiceCommentTableViewCell
        let comment = commentList[indexPath.row]
        cell.setData(comment: comment)
        return cell
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "reserve") {
            // pass data to next view
            let destinationVC = segue.destination as! ResrveRequestViewController
            destinationVC.service = service
        }
    }
    @IBAction func addToFavorites(_ sender: Any) {
        let token = TokenCache.get()
        if  !token.isEmpty{
             setFavorite()
        } else {
            let viewController:UIViewController = UIStoryboard(name: Constant.UserAuthStoryBoardIdentifier, bundle: nil).instantiateViewController(withIdentifier: Constant.UserAuthStoryBoardIdentifier) as UIViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    @IBAction func bookNow(_ sender: Any) {
        let token = TokenCache.get()
        if  !token.isEmpty{
            
            performSegue(withIdentifier: "reserve", sender: self)
            
//            let popOverVC = UIStoryboard(name: "Saloons", bundle: nil).instantiateViewController(withIdentifier: "reserve") as! ResrveRequestViewController
//            popOverVC.service = service
//            self.addChild(popOverVC)
//            popOverVC.view.frame = self.view.frame
//            self.view.addSubview(popOverVC.view)
//            popOverVC.didMove(toParent: self)
            
            
        } else {
            let viewController:UIViewController = UIStoryboard(name: Constant.UserAuthStoryBoardIdentifier, bundle: nil).instantiateViewController(withIdentifier: Constant.UserAuthStoryBoardIdentifier) as UIViewController
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sizeHeaderToFit()
    }
    
    func sizeHeaderToFit() {
        let headerView = tableView.tableHeaderView!
        
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        
        let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
        var frame = headerView.frame
        frame.size.height = height
        headerView.frame = frame
        
        tableView.tableHeaderView = headerView
    }
    
    func getServiceData(){
        showDialog(vc: self)
        RequestAPI.shared.getSaloonSeviceDetails(serviceId: serviceId){ result in
            switch result {
            case let .success(service):
                self.service = service
                print(service)
                self.setData()
                self.dismissDialog()
                
                break
            case let .failure(error):
                self.dismissDialog()
                Utlities.showAlert(vc: self, msg:error)
                break
            }
        }
    }
    
    func setFavorite(){
        showDialog(vc: self)
        RequestAPI.shared.setFavorite(id: serviceId,status:true){ result in
            switch result {
            case let .success(msg):
                self.dismissDialog()
                if let txt = msg {
                    Utlities.showAlert(vc: self, title: Constant.success, msg: txt)
                }
                break
            case let .failure(error):
                self.dismissDialog()
                Utlities.showAlert(vc: self, msg:error)
                break
            }
        }
    }
    
    func setData() {
        if let item = service {
            if let images = item.images , images.count > 0 {
                Utlities.setImage(imageView: sliderImageView, url: images[0].image)
                Utlities.setImage(imageView: firstImageInSlider, url: images[0].image)
                var i = 1
                while i < images.count {
                    let imageView = UIImageView()
                    sliderStackView.addSubview(imageView)
                    Utlities.setImage(imageView: imageView, url: images[i].image)
                    i = i + 1
                }
                self.serviceRatingBar.rating = Double(item.rate)
                self.serviceRatingBar.text = Utlities.getCurrentValueForLang(str: item.numberOfReviews)
                priceLabel.text = item.price
                descriptionLabel.text = item.description
                seatsLabel.text = "\(item.seats) \(Constant.seats)"
                timeLabel.text = "\(Constant.workingTime) : \(item.openingTimeFrom) - \(item.openingTimeTo)"
                dayLabel.text = "\(Constant.workingDay) : \(Constant.Days[item.openingDayFrom-1]) - \(Constant.Days[item.openingDayTo-1])"
                saloonNameLabel.text = Constant.saloonName + " : " + item.saloonName
                commentList = item.comments ?? []
                tableView.dataSource = self
                tableView.delegate = self
                if let subCategory = item.subCategory{
                 title = subCategory.name
                }
                
                if let service = service , service.latitude != 0.0 && service.longitude != 0.0 {
                    
            
                    
                let span = MKCoordinateSpan(latitudeDelta: 0.050, longitudeDelta: 0.050)
                    let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: service.latitude, longitude: service.longitude), span: span)
                mapView.setRegion(region, animated: true)
                let annotation = MKPointAnnotation()
                let centerCoordinate = CLLocationCoordinate2D(latitude: service.latitude, longitude:service.longitude)
                annotation.coordinate = centerCoordinate
                mapView.addAnnotation(annotation)
                }
            }
        }
    }
    
}
