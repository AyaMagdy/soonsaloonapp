//
//  SaloonDetailsReviewsTableViewCell.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/7/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit
import Cosmos

class SaloonDetailsReviewsTableViewCell: UITableViewCell {

    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var userNameLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var commentLabel: UILabel!
    
    
    func setData(comment : Comment?)
    {
        if let item = comment {
            rateView.settings.updateOnTouch = false
            rateView.rating = Double(item.rate) ?? 0
            userNameLabel.text = item.userName
            dateLabel.text = item.date
            commentLabel.text = item.comment
        }
     
    }

}
