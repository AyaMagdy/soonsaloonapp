//
//  UserCach.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/1/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation

class UserCache {
    static let key = "userCache"
    static func save(_ value: User!) {
        UserDefaults.standard.set(try? PropertyListEncoder().encode(value), forKey: key)
    }
    static func get() -> User! {
        var userData: User!
        if let data = UserDefaults.standard.value(forKey: key) as? Data {
            userData = try? PropertyListDecoder().decode(User.self, from: data)
            return userData!
        } else {
            return userData
        }
    }
    static func remove() {
        UserDefaults.standard.removeObject(forKey: key)
    }
}

class TokenCache {
    static let key = "tokenCache"
    static func save(_ value: String!) {
        print("my token save is \(value)")
        UserDefaults.standard.set(value, forKey: key)
    }
    
    static func get() -> String {
        print("my token get is \(UserDefaults.standard.string(forKey: key) ?? "")")
        return  UserDefaults.standard.string(forKey: key) ?? ""
    }
    static func remove() {
        UserDefaults.standard.removeObject(forKey: key)
    }
}

class UserTypeCache {
    static let key = "userTypeCache"
    static func save(_ value: UserType) {
        switch value {
        case .customer :
            UserDefaults.standard.set(1, forKey: key)
            break
        case .saloon :
            UserDefaults.standard.set(2, forKey: key)
            break
        }
    }
    
    static func get() -> UserType {
        let type = UserDefaults.standard.integer(forKey: key)
        return type == 1 ? .customer : .saloon
    }
    static func remove() {
        UserDefaults.standard.removeObject(forKey: key)
    }
}
