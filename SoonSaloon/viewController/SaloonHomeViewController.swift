//
//  SaloonHomeViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/25/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//
import UIKit
import TableViewReloadAnimation

class Service {
    var image : UIImage
    var name : String
    var seats : String
    
    init(image : UIImage ,name : String ,seats : String ) {
        self.image = image
        self.name = name
        self.seats = seats
        
    }
}

class SaloonHomeViewController: BaseViewController , UITableViewDelegate , UITableViewDataSource{

    @IBOutlet weak var serviceTableView: UITableView!
    var allService : [SaloonService] = []
    var selectIndex : Int?
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)

        // rotation spting animation
        serviceTableView.reloadData(
            with: .simple(duration: 0.75, direction: .rotation3D(type: .ironMan),
                          constantDelay: 0))
        
        refreshControl.addTarget(self, action: #selector(getServices), for: .valueChanged)
        serviceTableView.addSubview(refreshControl)
        
        getServices()
    }

    
    @objc func getServices(){
        refreshControl.beginRefreshing()
        //showDialog(vc: self)
        RequestAPI.shared.getSaloonSevice()
            { result in
                switch result {
                case let .success(services):
                    //self.dismissDialog()
                    self.refreshControl.endRefreshing()
                    if let items = services , items.list.count > 0 , items.list[0].id > 0 {
                        self.allService = items.list
                        self.serviceTableView.reloadData()

                    }else {
                        Utlities.showAlert(vc: self, msg:Constant.no_data)
                    }
                    print(self.allService)

                    break
                case let .failure(error):
                    self.refreshControl.endRefreshing()
                    Utlities.showAlert(vc: self, msg:error)
                    break
                }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allService.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SaloonServiceCell" ) as! SaloonHomeTableViewCell
        let service = allService[indexPath.row]
        cell.setService(service : service)
        return cell
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delete = UITableViewRowAction(style: .destructive, title: "Delete") { (action, indexPath) in
            // delete item at indexPath
            self.popUp(position : indexPath.row )
        }
        
        let edit = UITableViewRowAction(style: .default, title: "Edit") { (action, indexPath) in
            
            DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                let editServiceVC = UIStoryboard(name: "EditService", bundle: nil).instantiateViewController(withIdentifier: "editService") as!
                    EditServiceViewController
                
                editServiceVC.serviceObj  = self.allService[indexPath.row]
                self.navigationController?.pushViewController(editServiceVC, animated: true)
            }
        }
        
        edit.backgroundColor = UIColor.black
        delete.backgroundColor = #colorLiteral(red: 0.6107296944, green: 0.1066358462, blue: 0.2373355627, alpha: 1)
        return [delete, edit]
    }
    
    func popUp(position : Int ){
        let id = self.allService[position].id
        
        let alert = UIAlertController(title: Constant.deleteServicePopTitle, message: Constant.deleteServicePopMessage , preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: Constant.alertAccept, style: UIAlertAction.Style.default, handler: { (alert: UIAlertAction!) -> Void in
                self.deleteService(requestId : id , position : position)
        }))
        
        alert.addAction(UIAlertAction(title: Constant.alertReject, style: UIAlertAction.Style.default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    func deleteService(requestId : Int , position : Int)
    {
        showDialog(vc: self)
        RequestAPI.shared.deleteService(serviceId: requestId)
        { result in
            switch result {
            case .success(_):
                self.dismissDialog()
                self.allService.remove(at: position)
                self.serviceTableView.reloadData(
                    with: .simple(duration: 0.75, direction: .rotation3D(type: .ironMan),
                                  constantDelay: 0))
                break
            case let .failure(error):
                self.dismissDialog()
                DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1, execute: {
                    Utlities.showAlert(vc: self, msg:error)
                })
                break
            }
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectIndex = indexPath.row
       performSegue(withIdentifier: "serviceDetails", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "serviceDetails"{
            let destinationVC = segue.destination as! ServiceDetailsViewController
            destinationVC.id = allService[self.selectIndex!].id
        }
    }
}

