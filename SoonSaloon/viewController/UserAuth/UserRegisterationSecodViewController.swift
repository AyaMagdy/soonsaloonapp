//
//  UserRegisterationSecodViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/25/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class UserRegisterationSecodViewController: BaseViewController {
    
    @IBOutlet weak var confirmPawwordTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    var user:User?
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    @IBAction func finishAction(_ sender: Any) {
        if  nameTextField.text == "" || phoneTextField.text == "" || passwordTextField.text == "" || confirmPawwordTextField.text == "" {
            Utlities.showAlert(vc: self, msg: Constant.fillAllForm)
        }
        else{
            user?.phone = phoneTextField.text!
            user?.name = nameTextField.text!
            userRegister()
        }
    }
    
    func userRegister(){
        showDialog(vc: self)
        RequestAPI.shared.userRegister(password_confirmation : confirmPawwordTextField.text! ,password : passwordTextField.text! , userObj : user!){ result in
            switch result {
            case let .success(userData):
                UserCache.save(userData?.user)
                TokenCache.save(userData?.token)
                UserTypeCache.save(UserType.customer)
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                    let viewController:UIViewController = UIStoryboard(name: Constant.UserSideMenuIdentifier, bundle: nil).instantiateViewController(withIdentifier: Constant.userHomeIdentifier) as UIViewController
                    self.dismiss(animated: false, completion:   {
                        self.present(viewController, animated: false, completion: nil)
                    })
                }
                self.dismissDialog()
                break
            case let .failure(error):
                self.dismissDialog()
                DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1, execute: {
                    Utlities.showAlert(vc: self, msg:error)
                })
                break
            }
        }
    }
    
}
