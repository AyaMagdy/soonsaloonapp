//
//  splashViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/22/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class SplashViewController: UIViewController ,LocationProtocol {

    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = UIActivityIndicatorView.Style.gray
        activityIndicator.startAnimating()
        
        checkUser()
    }
    
    func checkUser() {
        if let user = UserCache.get(), !user.phone.isEmpty {
            DispatchQueue.main.async {
                if UserTypeCache.get() == .saloon {
                    self.openStoryBoard(storyBoardIdentifierName: Constant.SaloonSideMenuStoryBoardIdentifier, ControllerIdentifierName: Constant.SaloonHomeIdentifier)
                }
                else{
                    self.openStoryBoard(storyBoardIdentifierName: Constant.UserSideMenuIdentifier, ControllerIdentifierName: Constant.userHomeIdentifier)
                }
            }
        }else {
            DispatchQueue.main.async {
                self.openStoryBoard(storyBoardIdentifierName: Constant.UserSideMenuIdentifier, ControllerIdentifierName: Constant.userHomeIdentifier)
            }
        }
    }
    
    func openStoryBoard(storyBoardIdentifierName : String , ControllerIdentifierName:String) {
        let viewController:UIViewController = UIStoryboard(name: storyBoardIdentifierName, bundle: nil).instantiateViewController(withIdentifier: ControllerIdentifierName) as UIViewController
        self.present(viewController, animated: false, completion: nil)
    }
    
    
    ///// map
    @IBAction func openMapAction(_ sender: Any) {
        performSegue(withIdentifier: "goToMap", sender: self)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "goToMap"{
            let destinationVC = segue.destination as! MapViewController
             destinationVC.locationProtocol = self
        }
    }
    

    func completePin(latitude: Double, longitude: Double) {
       print("from splash lat is \(latitude) and long is \(longitude)")
    }
}
