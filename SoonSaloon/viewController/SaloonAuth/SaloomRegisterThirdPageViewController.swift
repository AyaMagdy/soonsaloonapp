//
//  SaloomRegisterThirdPageViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/25/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class SaloomRegisterThirdPageViewController: BaseViewController ,UIImagePickerControllerDelegate , UINavigationControllerDelegate{

    @IBOutlet weak var saloonImageView: UIImageView!
    
    var imagePicker = UIImagePickerController()
   
    var user:User?
    var password : String?
  
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imagePicker.delegate = self
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(selectImage))
        self.saloonImageView.isUserInteractionEnabled = true
        self.saloonImageView.addGestureRecognizer(gesture)

    }
    
    @objc func selectImage() {
        Utlities.selectImageFromGallery(vc: self, imagePicker: imagePicker)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info:[UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            saloonImageView.image = image
        }
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func finishAction(_ sender: Any) {
        showDialog(vc: self)
        RequestAPI.shared.saloonRegister(profile_image: self.saloonImageView.image!,password : password! , userObj : user!){ result in
            switch result {
            case let .success(userData):
                UserCache.save(userData?.user)
                TokenCache.save(userData?.token)
                UserTypeCache.save(UserType.saloon)
                DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                    let viewController:UIViewController = UIStoryboard(name: Constant.SaloonSideMenuStoryBoardIdentifier, bundle: nil).instantiateViewController(withIdentifier: Constant.SaloonHomeIdentifier) as UIViewController
                    self.dismiss(animated: false, completion:   {
                        self.present(viewController, animated: false, completion: nil)
                    })
                }
                self.dismissDialog()
                break
            case let .failure(error):
                self.dismissDialog()
                DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1, execute: {
                    Utlities.showAlert(vc: self, msg:error)
                })
                break
            }
        }
    }
}
