//
//  Request.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/9/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation

struct Request: Codable {
    var id = 0
    var date = ""
    var time = ""
    var status = ""
    
    var statusCode = ""
    var customerName = ""
    var customerPhone = ""
    var serviceId = 0
    var requestMessage = ""
    var rate = ""
    var saloonAddress = ""
    var saloonName = ""
    var saloonPhone = ""
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case date = "date"
        case time = "time"
        case status = "status"
        
        case statusCode = "status_code"
        case customerName = "customer_name"
        case customerPhone = "customer_phone"
        case serviceId = "service_id"
        case requestMessage = "request_message"
        case rate = "rate"
        case saloonAddress = "saloon_address"
        case saloonName = "saloon_name"
        case saloonPhone = "saloon_phone"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        self.date = try container.decodeIfPresent(String.self, forKey: .date) ?? ""
        self.time = try container.decodeIfPresent(String.self, forKey: .time) ?? ""
        self.status = try container.decodeIfPresent(String.self, forKey: .status) ?? ""
        
        self.statusCode = try container.decodeIfPresent(String.self, forKey: .statusCode) ?? ""
        self.customerName = try container.decodeIfPresent(String.self, forKey: .customerName) ?? ""
        self.customerPhone = try container.decodeIfPresent(String.self, forKey: .customerPhone) ?? ""
        self.serviceId = try container.decodeIfPresent(Int.self, forKey: .serviceId) ?? 0
        
        self.requestMessage = try container.decodeIfPresent(String.self, forKey: .requestMessage) ?? ""
        self.rate = try container.decodeIfPresent(String.self, forKey: .rate) ?? ""
        self.saloonAddress = try container.decodeIfPresent(String.self, forKey: .saloonAddress) ?? ""
        self.saloonName = try container.decodeIfPresent(String.self, forKey: .saloonName) ?? ""
        self.saloonPhone = try container.decodeIfPresent(String.self, forKey: .saloonPhone) ?? ""
        
    }
}
