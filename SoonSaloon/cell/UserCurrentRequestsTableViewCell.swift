//
//  UserCurrentRequestsTableViewCell.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/13/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class UserCurrentRequestsTableViewCell: UITableViewCell {

    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var timeTextField: UILabel!
    @IBOutlet weak var dateTextField: UILabel!
    @IBOutlet weak var descriptionTextField: UILabel!
    @IBOutlet weak var phoneTextField: UILabel!
    @IBOutlet weak var nameTextField: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    
    @IBOutlet weak var statusTextField: UILabel!
    
    var cellProtocol:CellProtocolIndex?
    var index = 0
    
    func setRequest(requests : Request){
        
        timeTextField.text = requests.time
        locationLabel.text = requests.saloonAddress
        dateTextField.text = requests.date
        descriptionTextField.text = requests.requestMessage
        phoneTextField.text = requests.customerPhone
        statusTextField.text = requests.status
        nameTextField.text = requests.customerName
        if (requests.statusCode == "\(1)")
        {
            editBtn.isHidden = false
        }
        else if requests.statusCode == "\(2)"
        {
            editBtn.isHidden = true
            statusTextField.textColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
        }
        
    }

    @IBAction func cancelAction(_ sender: Any) {
        cellProtocol?.click(position: index, type: .cancel)
    }
    @IBAction func editAction(_ sender: Any) {
        cellProtocol?.click(position: index, type: .edit)
    }

}
