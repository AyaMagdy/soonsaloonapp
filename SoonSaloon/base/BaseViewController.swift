//
//  BaseViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/5/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    var alert :UIAlertController?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func showDialog(vc:UIViewController) {
        alert = UIAlertController(title: nil, message: "Please Wait", preferredStyle: .alert)
        let loadingIndicator = UIActivityIndicatorView(frame: CGRect(x: 10, y: 5, width: 50, height: 50))
        loadingIndicator.hidesWhenStopped = true
        loadingIndicator.style = UIActivityIndicatorView.Style.gray
        loadingIndicator.startAnimating()
        if let myAlert = alert {
            myAlert.view.addSubview(loadingIndicator)
            present(myAlert, animated: true, completion: nil)
        }
        
    }
    
    func dismissDialog() {
        if let myAlert = alert {
            DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1, execute: {
                myAlert.dismiss(animated: true, completion: nil)
            })
        }
    }
}
