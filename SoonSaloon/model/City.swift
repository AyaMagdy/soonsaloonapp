//
//  City.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/2/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation

struct City: Codable {
    let id :Int?
    let name :String?
}
