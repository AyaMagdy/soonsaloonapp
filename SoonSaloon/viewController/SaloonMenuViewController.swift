//
//  SaloonMenuViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/25/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class SaloonMenuViewController: BaseViewController {

    @IBOutlet weak var saloonImageImageView: UIImageView!
    
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        if let user = UserCache.get(){
            Utlities.setImage(imageView: saloonImageImageView, url: user.profileImage)
            nameLabel.text = user.name
            phoneLabel.text = user.phone
        }
    }
    
    
    @IBAction func logout(_ sender: Any) {
        
        let refreshAlert = UIAlertController(title: Constant.logout, message: Constant.areYouSure, preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: Constant.yes, style: .default, handler: { (action: UIAlertAction!) in
            UserTypeCache.remove()
            UserCache.remove()
            TokenCache.remove()
            // request log out without loading or show error msg
            // open splash screen and clear stack
            self.logout()
            self.logoutfromStack()

        }))
        
        refreshAlert.addAction(UIAlertAction(title: Constant.no, style: .cancel, handler: { (action: UIAlertAction!) in
           self.dismiss(animated: true, completion: nil)
        }))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    
    func logout()
    {
        RequestAPI.shared.logout()
            { result in
                switch result {
                case .success(_):
                
                    break
                case let .failure(error):
                    Utlities.showAlert(vc: self, msg:error)
                    break
                }
        }

    }
    
   func logoutfromStack()
   {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
    
        // Should remove all subsequent view controllers from memory.
        appDelegate.window?.rootViewController!.dismiss(animated: true, completion: nil)
    
        // Set the root view controller to a new instance of the sign in view controller.
        appDelegate.window?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SplashViewController")
   }
    
    
    @IBAction func closeMenuAction(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
    }
    
}
