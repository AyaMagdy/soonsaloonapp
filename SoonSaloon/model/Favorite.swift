//
//  Favorite.swift
//  SoonSaloon
//
//  Created by alexlab on 5/15/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation

struct Favorite: Codable {
    var name = ""
    var description = ""
    var time = ""
    var serviceId = 0
    
    enum CodingKeys: String, CodingKey {
        case name = "saloon_name"
        case description = "service_name"
        case time = "time"
        case serviceId = "service_id"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        self.description = try container.decodeIfPresent(String.self, forKey: .description) ?? ""
        self.time = try container.decodeIfPresent(String.self, forKey: .time) ?? ""
        self.serviceId = try container.decodeIfPresent(Int.self, forKey: .serviceId) ?? 0
    }
}
