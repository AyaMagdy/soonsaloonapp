//
//  AddServiceViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/30/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit
import BSImagePicker
import Photos
class AddServiceViewController: BaseViewController , UICollectionViewDelegate ,UICollectionViewDataSource,UIImagePickerControllerDelegate ,UINavigationControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource {
   
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var descriptionTextFIield: UITextField!
    @IBOutlet weak var seatsTextField: UITextField!
    @IBOutlet weak var subCategoryTextField: UITextField!
    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var serviceCollectionView: UICollectionView!
    var selectedAssets = [PHAsset]()
    var photoArray = [UIImage]()
    
    var picker = UIPickerView()
    
    var categories = [MainCategory]()
    var subCategory = [SubCategories]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getCategoriesAndSub()
        

        picker.dataSource = self
        picker.delegate = self
        categoryTextField.inputView = picker
        subCategoryTextField.inputView = picker
        
    }

    @IBAction func addPhotoesAction(_ sender: Any) {
        selectedAssets = []
        let vc = BSImagePickerViewController()
        self.bs_presentImagePickerController(vc, animated: true,
            select: {(asset : PHAsset)->Void in

            }, deselect: {
                (asset : PHAsset)->Void in
                
            }, cancel: {
                (assets : [PHAsset])->Void in
                
            }, finish: {(assets : [PHAsset])->Void in
                for i in 0..<assets.count{
                    self.selectedAssets.append(assets[i])
                }
                self.convertAssetToImages()
            }, completion: nil)
    }
    
    func convertAssetToImages()->Void{
        if selectedAssets.count != 0 {
            for i in 0..<selectedAssets.count{
                let manager = PHImageManager.default()
                let option = PHImageRequestOptions()
                var thumbnail = UIImage()
                option.isSynchronous = true
                manager.requestImage(for: selectedAssets[i], targetSize: CGSize(width: 70, height: 70), contentMode: .aspectFill, options: option, resultHandler: {(result, info)->Void in
                    thumbnail = result!
                })
                let data = thumbnail.jpegData(compressionQuality: 0.7)
                let newImage = UIImage(data: data!)
                if self.photoArray.count < 5 {
                self.photoArray.append(newImage! as UIImage)
                }
                else{
                     Utlities.showAlert(vc: self, msg: "Can Not Add More Than 5 Image")
                }
              }
            DispatchQueue.main.async {
                self.serviceCollectionView.reloadData()
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photoArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "addServiceCell", for: indexPath) as! AddServiceCollectionViewCell
        cell.addServiceImageView.image = photoArray[indexPath.row]
        
        let UpSwipe = UISwipeGestureRecognizer(target: self, action: #selector(reset(_:)) )
        UpSwipe.direction = UISwipeGestureRecognizer.Direction.up
        cell.addGestureRecognizer(UpSwipe)
        return cell
    }
    
    // to put 3 photo in cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/3.0
        let yourHeight = collectionView.bounds.height
        return CGSize(width: yourWidth, height: yourHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    // end put 3 photo in cell
    @objc func reset(_ sender:UISwipeGestureRecognizer) {
        let cell = sender.view as! UICollectionViewCell
        let i = self.serviceCollectionView.indexPath(for: cell)!.item
        photoArray.remove(at: i)
        self.serviceCollectionView.reloadData()
    }

    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if categoryTextField.isFirstResponder {
            return categories.count
        }else if subCategoryTextField.isFirstResponder {
            return subCategory.count
        }else {
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if categoryTextField.isFirstResponder {
            subCategory = categories[row].subCategories!
            self.subCategoryTextField.text = subCategory.first?.name
            return categories[row].categoryName
        }else if subCategoryTextField.isFirstResponder {
            return subCategory[row].name
        }else {
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if categoryTextField.isFirstResponder {
            categoryTextField.text = categories[row].categoryName
        }else if subCategoryTextField.isFirstResponder {
            subCategoryTextField.text = subCategory[row].name
        }
    }
    
    func getCategoriesAndSub()
    {
        showDialog(vc: self)
        RequestAPI.shared.getCategoriesAndSubCategories()
            { result in
                switch result {
                case let .success(categories):
                    self.dismissDialog()
                    self.categories = categories!
                    self.subCategory = (categories?.first?.subCategories)!
                    self.categoryTextField.text = self.categories.first?.categoryName
                    self.subCategoryTextField.text = self.categories.first?.subCategories?.first?.name
                    break
                case let .failure(error):
                    self.dismissDialog()
                    DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1, execute: {
                        Utlities.showAlert(vc: self, msg:error)
                    })
                    break
                }
            }
    }
    @IBAction func addServiceAction(_ sender: Any) {
        if  seatsTextField.text == "" || subCategoryTextField.text == "" || categoryTextField.text == "" ||
            priceTextField.text == "" ||
            photoArray.count == 0  {
            Utlities.showAlert(vc: self, msg: Constant.fillAllForm)
        }
        else{
            addService()
        }
    }
    func addService(){
        showDialog(vc: self)
        let index = subCategory.index(where: { $0.name == subCategoryTextField.text })
        let subId = "\(subCategory[index!].id)"
        RequestAPI.shared.addService(description: descriptionTextFIield.text! ,seats : seatsTextField.text! , subCategory : subId , photos : photoArray, price: priceTextField.text!)
                 { result in
            switch result {
            case .success(_):
                self.dismissDialog()
                DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1, execute: {
                    self.priceTextField.text = ""
                    self.descriptionTextFIield.text = ""
                    self.seatsTextField.text = ""
                     Utlities.showAlert(vc: self,title:"Success", msg: Constant.serviceAdded)
                })
              
                break
            case let .failure(error):
                self.dismissDialog()
                DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1, execute: {
                    Utlities.showAlert(vc: self, msg:error)
                })
                break
            }
        }
    }
}
