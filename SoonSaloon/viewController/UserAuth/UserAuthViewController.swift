//
//  UserAuthViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/25/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class UserAuthViewController: BaseViewController {

    @IBOutlet weak var phoneTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func loginAction(_ sender: Any) {
        if let phone = phoneTextField.text , let password = passwordTextField.text {
            if phone.isEmpty || password.isEmpty {
                Utlities.showAlert(vc: self, msg: Constant.loginError)
            } else {
                showDialog(vc: self)
                RequestAPI.shared.login(phone: phone,password: password, userType: .customer){ result in
                    switch result {
                    case let .success(userData):
                        UserCache.save(userData?.user)
                        TokenCache.save(userData?.token)
                        UserTypeCache.save(UserType.customer)
                        DispatchQueue.main.asyncAfter(deadline: .now()+1) {
                            let viewController:UIViewController = UIStoryboard(name: Constant.UserSideMenuIdentifier, bundle: nil).instantiateViewController(withIdentifier: Constant.userHomeIdentifier) as UIViewController
                            self.dismiss(animated: false, completion:   {
                                self.present(viewController, animated: false, completion: nil)
                            })
                        }
                        self.dismissDialog()
                        self.navigationController?.popViewController(animated: true)
                        break
                    case let .failure(error):
                        self.dismissDialog()
                        Utlities.showAlert(vc: self, msg: error)
                        break
                    }
                    
                }
            }
        }
        
    }
}
