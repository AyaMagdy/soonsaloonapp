//
//  saloonAuthViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/23/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class SaloonAuthViewController: BaseViewController {
    
    @IBOutlet weak var phoneTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func loginAction(_ sender: Any) {
        if let phone = phoneTextField.text , let password = passwordTextField.text {
            if phone.isEmpty || password.isEmpty {
                Utlities.showAlert(vc: self, msg: Constant.loginError)
            } else {
                showDialog(vc: self)
                RequestAPI.shared.login(phone: phone,password: password, userType: .saloon){ result in
                    switch result {
                    case let .success(userData):
                        UserCache.save(userData?.user)
                        TokenCache.save(userData?.token)
                        UserTypeCache.save(UserType.saloon)

                        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                            let viewController:UIViewController = UIStoryboard(name: Constant.SaloonSideMenuStoryBoardIdentifier, bundle: nil).instantiateViewController(withIdentifier: Constant.SaloonHomeIdentifier) as  UIViewController
                            self.dismiss(animated: false, completion:   {
                                self.present(viewController, animated: false, completion: nil)
                            })
                        }
                        break
                    case let .failure(error):
                        self.dismissDialog()
                        DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1, execute: {
                            Utlities.showAlert(vc: self, msg:error)
                        })
                        break
                    }
                    
                }
            }
        }
        
    }
    
}
