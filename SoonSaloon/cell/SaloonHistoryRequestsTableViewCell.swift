//
//  SaloonHistoryRequestsTableViewCell.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/9/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class SaloonHistoryRequestsTableViewCell: UITableViewCell {

    @IBOutlet weak var timeTextField: UILabel!
    @IBOutlet weak var dateTextField: UILabel!
    @IBOutlet weak var descriptionTextField: UILabel!
    @IBOutlet weak var phoneTextField: UILabel!
    @IBOutlet weak var statusTextField: UILabel!
    @IBOutlet weak var nameTextField: UILabel!
   
    func setRequest(requests : Request){
        
        timeTextField.text = requests.time
        dateTextField.text = requests.date
        descriptionTextField.text = requests.requestMessage
        phoneTextField.text = requests.customerPhone
        statusTextField.text = requests.status
        nameTextField.text = requests.customerName
        
    }

}
