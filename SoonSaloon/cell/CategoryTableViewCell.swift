//
//  CategoryTableViewCell.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/28/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var NameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var seatsLabel: UILabel!
    
    func setData(category : CategoryAndSubCategory?)
    {
        if let item = category {
            Utlities.setImage(imageView: categoryImageView, url: item.image)
            //subCategorycategoryImageView.image = subCategory.image
            seatsLabel.text = Utlities.getCurrentValueForLang(str: item.numberOfSaloon)
            NameLabel.text = item.name
            descriptionLabel.text = item.description
        }
    }
    
}
