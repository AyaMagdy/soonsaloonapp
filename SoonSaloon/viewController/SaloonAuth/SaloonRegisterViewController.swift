//
//  saloonRegisterViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/24/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class SaloonRegisterViewController: BaseViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var fromTimeTextField: UITextField!
    @IBOutlet weak var toTimeTextField: UITextField!
    @IBOutlet weak var fromDateTextField: UITextField!
    @IBOutlet weak var toDateTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var countryTextField: UITextField!
    

    
    
    var countryIdSelected = 0
    var cityIdSelected = 0
    
    var allDays = Constant.Days
    
    var cityList = [City]()
    var countryList = [City]()

    
    var picker = UIPickerView()
    let datePickerView = UIDatePicker()
    let timePickerView = UIDatePicker()

    override func viewDidLoad() {
        super.viewDidLoad()
        picker.dataSource = self
        picker.delegate = self
        countryTextField.inputView = picker
        cityTextField.inputView = picker
        
        fromDateTextField.inputView = picker
        toDateTextField.inputView = picker
        
        showDateTimePicker()
        
        fromDateTextField.text = allDays.first
        toDateTextField.text = allDays.first
        
        getCountries()

    }

    @IBAction func selectCountryAction(_ sender: Any) {
        getCities(countryId: countryIdSelected)
        cityIdSelected = 0
    }
    
    
    
    func showDateTimePicker(){
        timePickerView.datePickerMode = UIDatePicker.Mode.time
        fromTimeTextField.inputView = timePickerView
        toTimeTextField.inputView = timePickerView
        timePickerView.addTarget(self, action: #selector(timePickerFromValueChanged), for: UIControl.Event.valueChanged)
       }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if countryTextField.isFirstResponder {
            return countryList.count
        }else if cityTextField.isFirstResponder {
            return cityList.count
        }else if fromDateTextField.isFirstResponder {
            return allDays.count
        }else if toDateTextField.isFirstResponder {
            return allDays.count
        }else {
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if countryTextField.isFirstResponder {
            return countryList[row].name
        }else if cityTextField.isFirstResponder {
            return cityList[row].name
        }else if fromDateTextField.isFirstResponder {
            return allDays[row]
        }else if toDateTextField.isFirstResponder {
            return allDays[row]
        }else {
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if countryTextField.isFirstResponder {
            countryTextField.text = countryList[row].name
            countryIdSelected = countryList[row].id ?? 0
        }else if cityTextField.isFirstResponder {
            cityTextField.text = cityList[row].name
            cityIdSelected = cityList[row].id ?? 0
        }else if fromDateTextField.isFirstResponder {
        return fromDateTextField.text = allDays[row]
        }else if toDateTextField.isFirstResponder {
        return toDateTextField.text = allDays[row]
        }
    }
    
    
    func getCountries()
    {
        showDialog(vc: self)
        RequestAPI.shared.getCountries()
            { result in
                switch result {
                case let .success(cities):
                    DispatchQueue.main.async {
                        self.dismissDialog()
                    }
                    self.countryList = cities!
                    break
                case let .failure(error):
                    self.dismissDialog()
                    Utlities.showAlert(vc: self, msg:error)
                    break
                }
        }
    }
    
    func getCities(countryId : Int)
    {
        showDialog(vc: self)
        RequestAPI.shared.allCities(CountryId: countryId)
            { result in
                switch result {
                case let .success(cities):
                    self.dismissDialog()
                    self.cityList = cities!
                    self.cityTextField.text = self.cityList.first?.name
                    break
                case let .failure(error):
                    self.dismissDialog()
                    Utlities.showAlert(vc: self, msg:error)
                    break
                }
        }
    }
    
    @objc func timePickerFromValueChanged(sender:UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        
        if fromTimeTextField.isFirstResponder {
            fromTimeTextField.text = dateFormatter.string(from: sender.date)
        }else if toTimeTextField.isFirstResponder {
            toTimeTextField.text = dateFormatter.string(from: sender.date)
        }
    }

    @IBAction func nextPageAction(_ sender: Any) {
        if  nameTextField.text == "" || cityTextField.text == "" || countryTextField.text == "" || fromTimeTextField.text == "" || toTimeTextField.text == "" || fromDateTextField.text == "" || toDateTextField.text == "" {
            Utlities.showAlert(vc: self, msg: Constant.fillAllForm)
        }
        else{
            performSegue(withIdentifier: Constant.nextToSecondRegisterPage, sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constant.nextToSecondRegisterPage {
            let destinationVC = segue.destination as! SaloomRegisterSecondPageViewController
            var userObj = User()
            userObj.name = nameTextField.text ?? ""
            userObj.city = "\(cityIdSelected)"
            userObj.country = "\(countryIdSelected)"
            userObj.model = Model()
            userObj.model?.openingTimeFrom = fromTimeTextField.text ?? ""
            userObj.model?.openingTimeTo = toTimeTextField.text ?? ""
            userObj.model?.openingDayFrom = allDays.index(of: fromDateTextField.text!)!+1
            userObj.model?.openingDayTo = allDays.index(of: toDateTextField.text!)!+1

            destinationVC.user = userObj
            
        }
    }
}


