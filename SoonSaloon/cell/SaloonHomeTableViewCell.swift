//
//  SaloonHomeTableViewCell.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/26/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit
import SDWebImage


class SaloonHomeTableViewCell: UITableViewCell {

    @IBOutlet weak var serviceImageView: UIImageView!
   
    @IBOutlet weak var serviceNameLabel: UILabel!
    @IBOutlet weak var seatsLabel: UILabel!
    
    
    func setService(service : SaloonService)
    {
       
        serviceImageView.sd_setImage(with: URL(string: ((service.subCategory?.image)!)), placeholderImage: UIImage(named: "btn 2"))
        seatsLabel.text = service.seats + " Seats"
        serviceNameLabel.text = service.subCategory?.name
       
    }

}
