//
//  ResrveRequestViewController.swift
//  SoonSaloon
//
//  Created by alexlab on 5/15/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class ResrveRequestViewController: BaseViewController {
    @IBOutlet var nameLabel: UILabel!
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var priceLabel: UILabel!
    
    @IBOutlet var seatsLabel: UILabel!
    
    @IBOutlet var dateTextField: UITextField!
    
    @IBOutlet var timeTextField: UITextField!
    
    
    var service:SaloonService?
    
    let datePicker = UIDatePicker()
    let timePicker = UIDatePicker()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        if let item = service {
            nameLabel.text = item.saloonName
            seatsLabel.text = "\(item.seats) \(Constant.seats)"
            priceLabel.text = item.price
            Utlities.setImage(imageView: imageView, url: item.images?[0].image ?? "")
        }
        
        datePicker.datePickerMode = UIDatePicker.Mode.date
        dateTextField.inputView = datePicker
        datePicker.addTarget(self, action: #selector(datePickerValueChanged), for: UIControl.Event.valueChanged)
        
        
        timePicker.datePickerMode = UIDatePicker.Mode.time
        timeTextField.inputView = timePicker
        timePicker.addTarget(self, action: #selector(timePickerValueChanged), for: UIControl.Event.valueChanged)
        
    }
    
    @objc func datePickerValueChanged(sender:UIDatePicker) {
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        
        dateTextField.text = dateFormatter.string(from: sender.date)
        
    }
    
    
    @objc func timePickerValueChanged(sender:UIDatePicker) {
        
        let timeFormatter = DateFormatter()
        
        timeFormatter.dateFormat = "HH:mm"
        
        
        timeTextField.text = timeFormatter.string(from: sender.date)
        
    }
    
    
    @IBAction func submitAction(_ sender: Any) {
        if let item = service {
            createRequest(serviceId: item.id, date: dateTextField.text ?? "", time: timeTextField.text ?? "")
        }
        
    }
    @IBAction func closeAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func createRequest(serviceId : Int , date : String , time : String)
    {
        
        showDialog(vc: self)
        RequestAPI.shared.createRequest(serviceId: serviceId, date: date ,time: time){ result in
            switch result {
            case let .success(msg):
                self.dismissDialog()
                self.dismiss(animated: true, completion: nil)
                Utlities.showAlert(vc: self, title: Constant.success, msg: msg ?? "")
                
                break
            case let .failure(error):
                self.dismissDialog()
                DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1, execute: {
                    Utlities.showAlert(vc: self, msg:error)
                })
                break
            }
        }
    }
    
}
