//
//  UserHistoryRequestsViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/13/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class UserHistoryRequestsViewController: BaseViewController, UITableViewDelegate , UITableViewDataSource ,CellProtocolWithIndex ,CallBackProtocol {
    
    
    @IBOutlet weak var historyTableView: UITableView!
    
    var allRequests : [Request] = []
    var refreshControl = UIRefreshControl()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "History"
        
        refreshControl.addTarget(self, action: #selector(getHistory), for: .valueChanged)
        historyTableView.addSubview(refreshControl)
        getHistory()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allRequests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyRequests" ) as! UserHistoryRequestsTableViewCell
        let request = allRequests[indexPath.row]
        cell.setRequest(requests : request)
        cell.cellProtocol = self
        cell.index = indexPath.row
        return cell
    }
    
    @objc func getHistory()
    {
        refreshControl.beginRefreshing()
        RequestAPI.shared.getRequestHistory()
            { result in
                switch result {
                case let .success(requests):
                    self.refreshControl.endRefreshing()
                    self.allRequests = requests!
                    self.historyTableView.reloadData(
                        with: .simple(duration: 0.75, direction: .rotation3D(type: .ironMan),
                                      constantDelay: 0))
                    break
                case let .failure(error):
                    self.refreshControl.endRefreshing()
                    DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1, execute: {
                        Utlities.showAlert(vc: self, msg:error)
                    })
                    break
                }
        }
    }
    
    func click(position: Int) {
        print("rate")
        let popOverVC = UIStoryboard(name: "UserRequests", bundle: nil).instantiateViewController(withIdentifier: "RequestRate") as! AddRequestRateViewController
        popOverVC.request = allRequests[position]
        popOverVC.index = position
        self.addChild(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParent: self)
        popOverVC.rateProtocol = self

    }
    
    func complete(responseObj: Comment, position: Int) {
        print("done")
        self.allRequests[position].rate = responseObj.rate
        self.historyTableView.reloadData(
            with: .simple(duration: 0.75, direction: .rotation3D(type: .ironMan),
                          constantDelay: 0))
    }
}
