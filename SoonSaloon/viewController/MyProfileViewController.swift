//
//  MyProfileViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/8/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class MyProfileViewController: UIViewController {
    @IBOutlet weak var saloonImageImageView: UIImageView!
    
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet var myView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = Constant.MyProfile
        
        if UserTypeCache.get() == .customer {
            myView.backgroundColor? = UIColor(named: "red") ?? UIColor.black
            navigationController?.navigationBar.barTintColor = UIColor(named: "red") ?? UIColor.black

        }else {
            myView.backgroundColor? = UIColor.black
            navigationController?.navigationBar.barTintColor = UIColor.black
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        if let user = UserCache.get(){
            Utlities.setImage(imageView: saloonImageImageView, url: user.profileImage)
            nameLabel.text = user.name
            phoneLabel.text = user.phone
        }
    }

    @IBAction func editProfileAction(_ sender: Any) {
        if UserTypeCache.get() == .saloon {
            performSegue(withIdentifier: "edit_saloon_profile", sender: self)
            
        }else {
           performSegue(withIdentifier: "edit_user_profile", sender: self)
        }
    }
}
