//
//  MapViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/20/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//
protocol LocationProtocol {
    func completePin(latitude :Double ,longitude : Double  )
}

import UIKit
import GoogleMaps
import MapKit

class MapViewController: BaseViewController , MKMapViewDelegate, UIGestureRecognizerDelegate{

    @IBOutlet weak var mapView: MKMapView!
    
    var locationProtocol : LocationProtocol!
    var lat : Double!
    var long :Double!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
//        let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: camera)
//        view = mapView
//        // Creates a marker in the center of the map.
//        let marker = GMSMarker()
//        marker.position = CLLocationCoordinate2D(latitude: -33.86, longitude: 151.20)
//        marker.title = "Sydney"
//        marker.snippet = "Australia"
//        marker.map = mapView
        
        mapView.delegate = self
        let gestureZ = UILongPressGestureRecognizer(target: self, action: #selector(self.revealRegionDetailsWithLongPressOnMap(sender:)))
        mapView.addGestureRecognizer(gestureZ)
        /// set location
        let span = MKCoordinateSpan(latitudeDelta: 0.050, longitudeDelta: 0.050)
        let region = MKCoordinateRegion(center: CLLocationCoordinate2D(latitude: 21.485811, longitude: 39.19250480000005), span: span)
        mapView.setRegion(region, animated: true)
    }

    @objc func revealRegionDetailsWithLongPressOnMap(sender: UILongPressGestureRecognizer) {
        if sender.state != UIGestureRecognizer.State.began { return }
        let touchLocation = sender.location(in: mapView)
        let locationCoordinate = mapView.convert(touchLocation, toCoordinateFrom: mapView)
        addPin(lat: locationCoordinate.latitude, long: locationCoordinate.longitude)
        lat = locationCoordinate.latitude
        long = locationCoordinate.longitude
    }
    
    func addPin (lat : Double , long : Double){
        removeAllAnnotations()
        let annotation = MKPointAnnotation()
        let centerCoordinate = CLLocationCoordinate2D(latitude: lat, longitude:long)
        annotation.coordinate = centerCoordinate
//        annotation.title = "Your Location"
        mapView.addAnnotation(annotation)
    }
    
    func removeAllAnnotations(){
        let annotations = mapView.annotations.filter {
            $0 !== self.mapView.userLocation
        }
        mapView.removeAnnotations(annotations)
    }
    
    func returnToBack(){
        self.locationProtocol?.completePin(latitude:lat, longitude: long)
         dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
  
    @IBAction func saveAction(_ sender: Any) {
        if (lat != nil) && (long != nil){
            returnToBack()
        }
        else{
            Utlities.showAlert(vc: self, msg: Constant.mapMessage)
        }
    }
    
}

