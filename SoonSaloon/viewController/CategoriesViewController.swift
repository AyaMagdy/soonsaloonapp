//
//  CategoriesViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/28/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//




import UIKit
import TableViewReloadAnimation

class CategoriesViewController: UIViewController , UICollectionViewDelegateFlowLayout , UICollectionViewDataSource , UITableViewDelegate , UITableViewDataSource{
    
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var categoryTableView: UITableView!
    
    var imgArr = [Banner]()
    var timer = Timer()
    var counter = 0
    
    var refreshControl = UIRefreshControl()
    var categories : [CategoryAndSubCategory] = []
    var selectedRow = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // timer to auto slide
        
        refreshControl.addTarget(self, action: #selector(getCategoriesAndSubCategories), for: .valueChanged)
        categoryTableView.addSubview(refreshControl)        
        getBanner()
    }
    // to auto slide
    @objc func changeImage(){
        if counter < imgArr.count {
            let index = IndexPath.init(item: counter, section: 0)
            self.collectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            counter += 1
        }
        else{
            counter = 0
            let index = IndexPath.init(item: counter, section: 0)
            self.collectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
            counter = 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imgArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath) as! CategoriesCollectionViewCell
        cell.setData(url: imgArr[indexPath.row].image)
        return cell
    }
    
    // to put 1 photo in cell and take view size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width
        let yourHeight = yourWidth
        
        return CGSize(width: yourWidth, height: yourHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    // end put 1 photo in cell
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let category = categories[indexPath.row]
        
        if (category.isMain){
            let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell" ) as! CategoryHeaderTableViewCell
            cell.setData(category: category)
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "categoryCell" ) as! CategoryTableViewCell
            cell.setData(category: category)
            return cell
        }
        
    }
    //    func numberOfSections(in tableView: UITableView) -> Int {
    //        return categories.count
    //    }
    
    //    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    //      return categories[section].categoryName
    //    }
    //    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
    ////        view.tintColor = UIColor.black
    //        let header = view as! UITableViewHeaderFooterView
    //        header.textLabel?.textColor = UIColor(named: "gray")
    //        // btn
    //        let DoneBut: UIButton = UIButton() //
    //        DoneBut.setTitle(Constant.viewAll, for: .normal)
    //        DoneBut.setTitleColor(UIColor(named: "gray"), for: .normal)
    //        DoneBut.titleLabel?.font = .systemFont(ofSize: 15)
    //
    //        DoneBut.frame = CGRect(x: tableView.frame.width - 55, y: 0, width: 50, height: 20)
    //
    ////        DoneBut.backgroundColor = UIColor.red
    //        DoneBut.tag = section
    //        selectedSection = section
    //        DoneBut.addTarget(self, action: #selector(test), for: .touchUpInside)
    //        header.addSubview(DoneBut)
    //
    //    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedRow = indexPath.row
        if (categories[indexPath.row].isMain){
            performSegue(withIdentifier: "show_sub_category", sender: self)
        }else {
            performSegue(withIdentifier: "show_service_in_saloon", sender: self)
        }
    }
    //
    //    @objc func test(){
    //        performSegue(withIdentifier: "show_sub_category", sender: self)
    //    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "show_sub_category") {
            // pass data to next view
            let destinationVC = segue.destination as! SaloonSubCategoryViewController
            destinationVC.categoryId = categories[selectedRow].id
            destinationVC.title = categories[selectedRow].name
        }else if (segue.identifier == "show_service_in_saloon") {
            // pass data to next view
            let destinationVC = segue.destination as! SaloonsViewController
            destinationVC.subCategoryId = categories[selectedRow].id
            destinationVC.title = categories[selectedRow].name
        }
    }
    
    @objc func getCategoriesAndSubCategories(){
        refreshControl.beginRefreshing()
        RequestAPI.shared.getCategoriesAndSubCategories()
            { result in
                switch result {
                case let .success(list):
                    self.categoryTableView.dataSource = self
                    self.categoryTableView.delegate = self
                    self.convertMainCatgorytoCategoryAndSubCategory(list: list ?? [])
                    self.refreshControl.endRefreshing()
                    self.categoryTableView.reloadData()
                    break
                case let .failure(error):
                    self.refreshControl.endRefreshing()
                    Utlities.showAlert(vc: self, msg:error)
                    break
                }
        }
    }
    
    
    @objc func getBanner(){
        refreshControl.beginRefreshing()
        RequestAPI.shared.getBanner()
            { result in
                switch result {
                case let .success(list):
                    self.imgArr = list ?? []
                    self.collectionView.reloadData()
                    self.playBanner()
                    self.getCategoriesAndSubCategories()
                    break
                case let .failure(error):
                    self.refreshControl.endRefreshing()
                    Utlities.showAlert(vc: self, msg:error)
                    break
                }
        }
    }
    
    func playBanner(){
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(self.changeImage), userInfo: nil, repeats: true)
        }
    }
    
    func convertMainCatgorytoCategoryAndSubCategory(list:[MainCategory]) {
        categories.removeAll()
        for item in list {
            categories.append(CategoryAndSubCategory(id: item.id, name: item.categoryName))
            for (i,subItem) in (item.subCategories?.enumerated())!{
                if (i >= 2){
                    break
                }
                categories.append(CategoryAndSubCategory(id: subItem.id, name: subItem.name, image: subItem.image, numberOfSaloon: subItem.numberOfSaloon, description: subItem.description))
            }
        }
    }
    
}
