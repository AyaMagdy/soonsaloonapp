//
//  AddRequestRateViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/13/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit
import Cosmos

protocol CallBackProtocol {
    func complete(responseObj: Comment ,position : Int )
}

class AddRequestRateViewController: BaseViewController {

    var request : Request?
    var comment : Comment?
    var index: Int?
    var rateProtocol : CallBackProtocol!
    var rate : Int = 0
    @IBOutlet weak var commentTextField: UITextField!

    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var rateView: CosmosView!
    override func viewDidLoad() {
        super.viewDidLoad()
        nameLabel.text = request?.saloonName
        phoneLabel.text = request?.saloonPhone
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        rateView.didFinishTouchingCosmos = { [weak self] rating in
            self!.rate = Int(rating)
            print(rating)
        }
        rateView.didTouchCosmos = { [weak self] rating in
            self!.rate = Int(rating)
            print(rating)
        }
    }
    

    
    @IBAction func closeAction(_ sender: Any) {
        self.view.removeFromSuperview()
    }

    @IBAction func saveAction(_ sender: Any) {
        
        if rate == 0 {
            Utlities.showAlert(vc: self, msg: Constant.fillRate)
        }
        else{
            addReview(requestId : (request?.id)! , comment : commentTextField.text ?? "" , rate : rate)
        }
    }

    
    func addReview(requestId : Int , comment : String , rate : Int)
    {
        print(requestId)
        showDialog(vc: self)
        RequestAPI.shared.addRequestRate(requestId: requestId, comment : comment ,rate: rate)
        { result in
            switch result {
            case let .success(comment):
                self.dismissDialog()
                self.comment = comment
                self.view.removeFromSuperview()
                self.rateProtocol?.complete(responseObj: comment!, position: self.index!)
                break
            case let .failure(error):
                self.dismissDialog()
                DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1, execute: {
                    Utlities.showAlert(vc: self, msg:error)
                })
                break
            }
        }
    }

}
