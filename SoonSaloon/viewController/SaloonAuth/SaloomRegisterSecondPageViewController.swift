//
//  SaloomRegisterSecondPageViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/25/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class SaloomRegisterSecondPageViewController: BaseViewController,LocationProtocol {
   
    

    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!

    var user:User?
    var lat = 0.0
    var lng = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
     

    }
    
    func completePin(latitude: Double, longitude: Double) {
        lat = latitude
        lng = longitude
        locationTextField.text = "\(latitude) , \(longitude)"
        Utlities.getAddressFromLatLon(lat: latitude, lon: longitude){result in
            switch result {
            case let .success(address):
                self.locationTextField.text = address
            case .failure(_):
                print("")
            }
        }
    }
    
    
    @IBAction func selectLocation(_ sender: Any) {
        let showItemStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let showItemNavController = showItemStoryboard.instantiateViewController(withIdentifier: "Map") as! MapViewController
        showItemNavController.locationProtocol = self
        present(showItemNavController, animated: true)
    }
 

    @IBAction func nextPageAction(_ sender: Any) {
        if  phoneTextField.text == ""  || passwordTextField.text == "" || confirmPasswordTextField.text == ""{
            
            Utlities.showAlert(vc: self, msg: Constant.fillAllForm)
        }
        else{
            if passwordTextField.text == confirmPasswordTextField.text{
                performSegue(withIdentifier: Constant.nextToThirdRegisterVC, sender: self)
            }
            else{
                Utlities.showAlert(vc: self, msg: Constant.passwordMatch)
            }
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == Constant.nextToThirdRegisterVC{
            let destinationVC = segue.destination as! SaloomRegisterThirdPageViewController
            
            if var currentUser = user {
            
            currentUser.phone = phoneTextField.text ?? ""
            currentUser.model?.latitude = lat
            currentUser.model?.longitude = lng
            destinationVC.password = passwordTextField.text
            destinationVC.user = currentUser
            }
         
        }
    }
}


