//
//  NotificationViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/28/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class NotificationViewController: BaseViewController, UITableViewDelegate , UITableViewDataSource {

    @IBOutlet weak var notificationTableView: UITableView!
    
    var allNotification : [Notification] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getNotifications()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allNotification.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell" ) as! NotificationTableViewCell
        let notificationData = allNotification[indexPath.row]
        cell.setService(notification : notificationData)
        return cell
    }
    
    func getNotifications()
    {
        showDialog(vc: self)
        RequestAPI.shared.allNotifications()
        { result in
            switch result {
            case let .success(notifications):
                self.dismissDialog()
                self.allNotification = notifications!
                self.notificationTableView.reloadData(
                    with: .simple(duration: 0.75, direction: .rotation3D(type: .ironMan),
                                  constantDelay: 0))
                break
            case let .failure(error):
                self.dismissDialog()
                DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1, execute: {
                    Utlities.showAlert(vc: self, msg:error)
                })
                break
            }
        }
    }
}
