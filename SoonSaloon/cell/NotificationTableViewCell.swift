//
//  NotificationTableViewCell.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/28/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

  
    
    func setService(notification : Notification)
    {
        nameLabel.text = notification.name
        timeLabel.text = notification.time
        bodyLabel.text = notification.message
    }

}
