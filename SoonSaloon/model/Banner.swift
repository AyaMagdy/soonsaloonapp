//
//  Banner.swift
//  SoonSaloon
//
//  Created by alexlab on 5/8/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation

struct Banner: Codable {
    var id = "0"
    var image = ""
    
    enum CodingKeys: String, CodingKey {
        case id = "service_id"
        case image = "image"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(String.self, forKey: .id) ?? "0"
        self.image = try container.decodeIfPresent(String.self, forKey: .image) ?? ""
    }
}
