//
//  editServiceCollectionViewCell.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/16/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class editServiceCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var addServiceImageView: UIImageView!
    
    func setData(image:ServiceImage){
        if (image.image != ""){
            // show url
            Utlities.setImage(imageView: addServiceImageView, url: image.image)
        }else {
            addServiceImageView.image = image.uiImage
        }
    }
}
