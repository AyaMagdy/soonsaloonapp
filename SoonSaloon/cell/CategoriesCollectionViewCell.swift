//
//  CategoriesCollectionViewCell.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/28/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit


//class ScaledHeightImageView: UIImageView {
//    
//    override var intrinsicContentSize: CGSize {
//        
//        if let myImage = self.image {
//            let myImageWidth = myImage.size.width
//            let myImageHeight = myImage.size.height
//            let myViewWidth = self.frame.size.width
//            
//            let ratio = myViewWidth/myImageWidth
//            let scaledHeight = myImageHeight * ratio
//            
//            return CGSize(width: myViewWidth, height: scaledHeight)
//        }
//        
//        return CGSize(width: -1.0, height: -1.0)
//    }
//    
//}

class CategoriesCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryImageView: UIImageView!
    
    
    func setData(url:String) {
          Utlities.setImage(imageView:categoryImageView, url: url)
    }
}
