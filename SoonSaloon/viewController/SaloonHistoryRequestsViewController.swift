//
//  SaloonHistoryRequestsViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/9/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class SaloonHistoryRequestsViewController:  BaseViewController, UITableViewDelegate , UITableViewDataSource  {

    @IBOutlet weak var historyTableView: UITableView!
    
        var allRequests : [Request] = []
        var refreshControl = UIRefreshControl()

        override func viewDidLoad() {
            super.viewDidLoad()
            self.navigationItem.title = "History"
            
            
            refreshControl.addTarget(self, action: #selector(getHistory), for: .valueChanged)
            historyTableView.addSubview(refreshControl)
            
            getHistory()
        }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allRequests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "historyRequests" ) as! SaloonHistoryRequestsTableViewCell
        let request = allRequests[indexPath.row]
        cell.setRequest(requests : request)
        return cell
    }
    
    @objc func getHistory()
    {
        refreshControl.beginRefreshing()
        RequestAPI.shared.getRequestHistory()
            { result in
                switch result {
                case let .success(requests):
                    self.allRequests = requests!
                    self.historyTableView.reloadData(
                        with: .simple(duration: 0.75, direction: .rotation3D(type: .ironMan),
                                      constantDelay: 0))
                    self.refreshControl.endRefreshing()
                    break
                case let .failure(error):
                    self.refreshControl.endRefreshing()
                    DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1, execute: {
                        Utlities.showAlert(vc: self, msg:error)
                    })
                    break
                }
        }
    }

}
