//
//  SaloonServiceCollectionViewCell.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/6/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit
import SDWebImage

class SaloonServiceCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var serviceImageView: UIImageView!
    
    
    func setService(serviceImage : ServiceImage)
    {
        Utlities.setImage(imageView: serviceImageView, url: serviceImage.image)
    }
}
