//
//  FavoritesViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/29/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//



import UIKit

class FavoritesViewController: BaseViewController ,UITableViewDataSource ,UITableViewDelegate,IFavoritesTableViewCell {

    @IBOutlet weak var favoriteTableView: UITableView!
    var allFavorite : [Favorite] = []
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //         rotation spting animation
        
        refreshControl.addTarget(self, action: #selector(getFavorite), for: .valueChanged)
        favoriteTableView.addSubview(refreshControl)
        getFavorite()
        favoriteTableView.tableFooterView = UIView()

    }
    
    
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allFavorite.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "favoriteCell" ) as! FavoritesTableViewCell
        let favorite = allFavorite[indexPath.row]
        cell.setFavorite(favorites : favorite)
        cell.index = indexPath.row
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        let showItemStoryboard = UIStoryboard(name: "Saloons", bundle: nil)
        let showItemNavController = showItemStoryboard.instantiateViewController(withIdentifier: "UserServiceDetails") as! UserServiceDetailsViewController
        showItemNavController.serviceId = allFavorite[indexPath.row].serviceId
        showItemNavController.title = allFavorite[indexPath.row].name
        // Set the properties in your showItemVC
        self.navigationController?.pushViewController(showItemNavController, animated: true)
    }
    
    
    func click(position: Int) {
        let refreshAlert = UIAlertController(title: Constant.delete, message: Constant.areYouSure, preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: Constant.yes, style: .default, handler: { (action: UIAlertAction!) in
            let serviceId = self.allFavorite[position].serviceId
            self.removeFavorite(serviceId: serviceId)
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: Constant.no, style: .cancel, handler: { (action: UIAlertAction!) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        present(refreshAlert, animated: true, completion: nil)
        
    }

    func removeFavorite(serviceId:Int){
        showDialog(vc: self)
        RequestAPI.shared.setFavorite(id: serviceId,status:false){ result in
            switch result {
            case .success(_):
                self.dismissDialog()
                self.getFavorite()
                break
            case let .failure(error):
                self.dismissDialog()
                Utlities.showAlert(vc: self, msg:error)
                break
            }
        }
    }
    
    @objc func getFavorite()
    {
        refreshControl.beginRefreshing()
        RequestAPI.shared.getFavorite()
            { result in
                switch result {
                case let .success(favorite):
                    if let items = favorite{
                    self.allFavorite = items
                  
                    self.refreshControl.endRefreshing()
                    self.favoriteTableView.reloadData(
                        with: .spring(duration: 0.45, damping: 0.65, velocity: 1, direction: .right(useCellsFrame: false),
                                      constantDelay: 0))
                    }
                    break
                case let .failure(error):
                    self.refreshControl.endRefreshing()
                    DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1, execute: {
                        Utlities.showAlert(vc: self, msg:error)
                    })
                    break
                }
        }
    }

}
