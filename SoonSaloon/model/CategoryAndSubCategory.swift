//
//  CategoryAndSubCategory.swift
//  SoonSaloon
//
//  Created by alexlab on 5/14/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation

struct CategoryAndSubCategory {
    
    var id = 0
    var name = ""
    var image = ""
    var numberOfSaloon : NumberOfReviews?
    var description = ""
    var isMain = true
    
    init(id:Int,name:String,image:String,numberOfSaloon:NumberOfReviews?,description:String) {
        self.id = id
        self.name = name
        self.image = image
        self.numberOfSaloon = numberOfSaloon
        self.description = description
        self.isMain = false
    }
    
    init(id:Int,name:String) {
        self.id = id
        self.name = name
        self.isMain = true
    }
    
}
