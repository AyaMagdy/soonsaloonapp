//
//  SaloonCurrentRequestsViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/9/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class SaloonCurrentRequestsViewController: BaseViewController, UITableViewDelegate , UITableViewDataSource , CellProtocolIndex ,EditProtocol {
 
    
    
    @IBOutlet weak var currentRequestTableView: UITableView!
    
    var allRequests : [Request] = []
    var refreshControl = UIRefreshControl()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Current"
        
        refreshControl.addTarget(self, action: #selector(getHistory), for: .valueChanged)
        currentRequestTableView.addSubview(refreshControl)
        
        getHistory()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allRequests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "currentCell" ) as! SaloonCurrentRequestsTableViewCell
        let request = allRequests[indexPath.row]
        cell.setRequest(requests : request)
        cell.index = indexPath.row
        cell.cellProtocol = self
        return cell
    }
    
    @objc func getHistory()
    {
        refreshControl.beginRefreshing()
        RequestAPI.shared.getRequestCurrent()
            { result in
                switch result {
                case let .success(requests):
                    
                    self.allRequests = requests!
                    self.currentRequestTableView.reloadData(
                        with: .simple(duration: 0.75, direction: .rotation3D(type: .ironMan),
                                      constantDelay: 0))
                    self.refreshControl.endRefreshing()
                    break
                case let .failure(error):
                    self.refreshControl.endRefreshing()
                    DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1, execute: {
                        Utlities.showAlert(vc: self, msg:error)
                    })
                    break
                }
        }
    }
    
    
    
    func click(position: Int, type: actionType) {
        let refreshAlert = UIAlertController(title: Constant.areYouSure, message: "", preferredStyle: UIAlertController.Style.alert)
        
        refreshAlert.addAction(UIAlertAction(title: Constant.yes, style: .default, handler: { (action: UIAlertAction!) in
            if (type == .edit)
            {
                let popOverVC = UIStoryboard(name: "SaloonRequests", bundle: nil).instantiateViewController(withIdentifier: "editRequest") as! EditRequestViewController
                popOverVC.request = self.allRequests[position]
                popOverVC.index = position
                popOverVC.serviceProtocol = self
                self.addChild(popOverVC)
                popOverVC.view.frame = self.view.frame
                self.view.addSubview(popOverVC.view)
                popOverVC.didMove(toParent: self)
                popOverVC.serviceProtocol = self
                
            }
            else if (type == .changeStatus)
            {
                self.popUp(title : Constant.changeStatusTitle, message: Constant.changeStatusMessage, type: .changeStatus , position : position)
            }
            else if type == .cancel {
                self.popUp(title : Constant.cancelTitle, message: Constant.cancelMessage, type: .cancel , position : position)
            }
            
        }))
        
        refreshAlert.addAction(UIAlertAction(title: Constant.no, style: .cancel, handler: { (action: UIAlertAction!) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        present(refreshAlert, animated: true, completion: nil)
    }
    
    func popUp(title : String , message: String , type : actionType , position : Int ){
        let id = self.allRequests[position].id
        
        let alert = UIAlertController(title: title, message: message , preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: Constant.alertAccept, style: UIAlertAction.Style.default, handler: { (alert: UIAlertAction!) -> Void in
            if (type == .cancel){
                self.CancelRequest(requestId : id , position : position)
            }
            else if type == .changeStatus{
                var statusCode = 0
                if self.allRequests[position].statusCode == "\(1)" {
                    statusCode = 2
                }
                else
                {
                    statusCode = 3
                }
                self.changeStatus(requestId : id , position : position , statusCode : statusCode)
            }
        }))
        
        alert.addAction(UIAlertAction(title: Constant.alertReject, style: UIAlertAction.Style.default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
    
    
    func changeStatus(requestId : Int , position : Int , statusCode : Int)
    {
        showDialog(vc: self)
        RequestAPI.shared.changeRequestStatus(requestId: requestId, statusCode: statusCode)
        { result in
            switch result {
            case let .success(request):
                self.dismissDialog()
                if let item = request {
                    self.allRequests[position] = item
                    self.currentRequestTableView.reloadData()
                }
                break
            case let .failure(error):
                self.dismissDialog()
                DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1, execute: {
                    Utlities.showAlert(vc: self, msg:error)
                })
                break
            }
        }
    }
    
    func CancelRequest(requestId : Int , position : Int)
    {
        showDialog(vc: self)
        RequestAPI.shared.cancelRequest(requestId: requestId)
        { result in
            switch result {
            case .success(_):
                self.dismissDialog()
                self.allRequests.remove(at: position)
                self.currentRequestTableView.reloadData(
                    with: .simple(duration: 0.75, direction: .rotation3D(type: .ironMan),
                                  constantDelay: 0))
                break
            case let .failure(error):
                self.dismissDialog()
                DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1, execute: {
                    Utlities.showAlert(vc: self, msg:error)
                })
                break
            }
        }
    }
    
  
    func completeEdit(requestObj: Request, position: Int) {
        self.allRequests[position] = requestObj
        self.currentRequestTableView.reloadData(
            with: .simple(duration: 0.75, direction: .rotation3D(type: .ironMan),
                          constantDelay: 0))
    }
    
}
