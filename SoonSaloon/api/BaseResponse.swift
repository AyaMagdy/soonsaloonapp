//
//  BaseResponse.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/30/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation

struct BaseResponse<T:Codable> : Codable {
    
    var status = false
    var message = ""
    var data:T?
    
}
struct BaseResponseString<T:Codable> : Codable {
    
    var status = false
    var message = ""
    var data = ""
    
}

struct PureResponse : Codable {
    
    var status = false
    var message = ""
    
}

struct BaseResponseArray<T:Codable> : Codable {
    
    var status = false
    var message = ""
    var data:[T]?
    
}
