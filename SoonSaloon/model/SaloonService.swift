//
//  Service.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/5/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation
import UIKit

struct ServiceResponse: Codable {
    let list: [SaloonService]
    
    enum CodingKeys: String, CodingKey {
        case list = "list"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.list = try container.decodeIfPresent([SaloonService].self, forKey: .list) ?? [SaloonService(from: decoder)]
    }
}

struct SaloonService:Codable {
    var id = 0
    var seats = ""
    var subCategory : SubCategories?
    var price = ""
    var priceNumber = ""
    var description = ""
    var images : [ServiceImage]?
    var numberOfReviews : NumberOfReviews?
    var openingTimeFrom = ""
    var openingTimeTo = ""
    var openingDayFrom = 0
    var openingDayTo = 0
    var rate = 0.0
    var saloonImage = ""
    var saloonName = ""
    var comments : [Comment]?
    var latitude = 0.0
    var longitude = 0.0
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case seats = "seats"
        case subCategory = "subCategory"
        case price = "price"
        case description = "description"
        case images = "images"
        case numberOfReviews = "number_of_reviews"
        case openingTimeFrom = "opening_time_from"
        case openingTimeTo = "opening_time_to"
        case openingDayFrom = "opening_day_from"
        case openingDayTo = "opening_day_to"
        case rate = "total_rating"
        case saloonImage = "saloon_image"
        case saloonName = "saloon_name"
        case comments = "ratings"
        case priceNumber = "price_number"
        case latitude = "latitude"
        case longitude = "longitude"
        
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        self.seats = try container.decodeIfPresent(String.self, forKey: .seats) ?? ""
        self.subCategory = try container.decodeIfPresent(SubCategories.self, forKey: .subCategory) ?? SubCategories(from: decoder)
        self.price = try container.decodeIfPresent(String.self, forKey: .price) ?? ""
        self.priceNumber = try container.decodeIfPresent(String.self, forKey: .priceNumber) ?? ""
        self.description = try container.decodeIfPresent(String.self, forKey: .description) ?? ""
        self.images = try container.decodeIfPresent([ServiceImage].self, forKey: .images) ?? [ServiceImage(from: decoder)]
        self.numberOfReviews = try container.decodeIfPresent(NumberOfReviews.self, forKey: .numberOfReviews) ?? NumberOfReviews(from: decoder)
        self.openingTimeFrom = try container.decodeIfPresent(String.self, forKey: .openingTimeFrom) ?? ""
        self.openingTimeTo = try container.decodeIfPresent(String.self, forKey: .openingTimeTo) ?? ""
        
        self.openingDayFrom = try container.decodeIfPresent(Int.self, forKey: .openingDayFrom) ?? 0
        self.openingDayTo = try container.decodeIfPresent(Int.self, forKey: .openingDayTo) ?? 0
        self.rate = try container.decodeIfPresent(Double.self, forKey: .rate) ?? 0.0
        self.saloonImage = try container.decodeIfPresent(String.self, forKey: .saloonImage) ?? ""
        self.saloonName = try container.decodeIfPresent(String.self, forKey: .saloonName) ?? ""
        self.comments = try container.decodeIfPresent([Comment].self, forKey: .comments) ?? [Comment(from: decoder)]
        self.latitude = try container.decodeIfPresent(Double.self, forKey: .latitude) ?? 0.0
        self.longitude = try container.decodeIfPresent(Double.self, forKey: .longitude) ?? 0.0
    }
}




struct ServiceImage:Codable {
    var image = ""
    var id = 0
    var uiImage:UIImage?
    
    
    init(image:String,id:Int) {
        self.image = image
        self.id = id
    }
    
    init(uiImage:UIImage) {
        self.uiImage = uiImage
    }
    
    
    enum CodingKeys: String, CodingKey {
        case image = "image_url"
        case id = "id"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.image = try container.decodeIfPresent(String.self, forKey: .image) ?? ""
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
    }
}

struct NumberOfReviews:Codable {
    var en = ""
    var ar = ""
    
    enum CodingKeys: String, CodingKey {
        case en = "en"
        case ar = "ar"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.en = try container.decodeIfPresent(String.self, forKey: .en) ?? ""
        self.ar = try container.decodeIfPresent(String.self, forKey: .ar) ?? ""
    }
}



