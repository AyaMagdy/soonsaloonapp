//
//  MainViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/4/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func UserAction(_ sender: Any) {
         Utlities.openVC(vc: self, identifier: "UserAuth")
    }
    
    @IBAction func saloonAction(_ sender: Any) {
         Utlities.openVC(vc: self, identifier: "SaloonAuth")
    }
    
}
