//
//  MainCategory.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/7/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation

struct MainCategory: Codable {
    var id = 0
    var categoryName = ""
    var subCategories : [SubCategories]?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case categoryName = "category_name"
        case subCategories = "sub_categories"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        self.categoryName = try container.decodeIfPresent(String.self, forKey: .categoryName) ?? ""
        self.subCategories = try container.decodeIfPresent([SubCategories].self, forKey: .subCategories) ?? [SubCategories(from: decoder)]
    }
}


struct SubCategories: Codable {
    var id = 0
    var name = ""
    var image = ""
    var numberOfSaloon : NumberOfReviews?
    var description = ""
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case image = "image"
        case numberOfSaloon = "number_of_saloons"
        case description = "description"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decodeIfPresent(Int.self, forKey: .id) ?? 0
        self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        self.image = try container.decodeIfPresent(String.self, forKey: .image) ?? ""
        self.numberOfSaloon = try container.decodeIfPresent(NumberOfReviews.self, forKey: .numberOfSaloon) ?? NumberOfReviews(from: decoder)
        self.description = try container.decodeIfPresent(String.self, forKey: .description) ?? ""
    }
}
