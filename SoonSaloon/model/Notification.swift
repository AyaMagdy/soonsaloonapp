//
//  Notification.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/7/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation

struct Notification: Codable {
    var name = ""
    var message = ""
    var time = ""
    var date = ""
    
    enum CodingKeys: String, CodingKey {
        case name = "name"
        case message = "message"
        case time = "time"
        case date = "date"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.name = try container.decodeIfPresent(String.self, forKey: .name) ?? ""
        self.message = try container.decodeIfPresent(String.self, forKey: .message) ?? ""
        self.time = try container.decodeIfPresent(String.self, forKey: .time) ?? ""
        self.date = try container.decodeIfPresent(String.self, forKey: .date) ?? ""
     }
}
