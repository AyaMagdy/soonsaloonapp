//
//  Utilities.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/22/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit
import SDWebImage
import CoreLocation

class Utlities {
    
    public static func setImage(imageView:UIImageView,url:String){
        if (url.isEmpty){
            return
        }
        imageView.sd_setImage(with: URL(string: url), placeholderImage: UIImage(named: "placehoulder.png"))
    }
    
    static func openVC(vc:UIViewController,identifier:String){
        let storyBoard = UIStoryboard(name: identifier, bundle: nil)
        let mainViewController = storyBoard.instantiateViewController(withIdentifier:identifier)
        vc.navigationController?.pushViewController(mainViewController, animated: true)
    }
    
    static func openVCInStoryBoard(vc:UIViewController,StoryBoardIdentifier:String ,controllerIdentifier :String){
        let storyBoard = UIStoryboard(name: StoryBoardIdentifier, bundle: nil)
        let mainViewController = storyBoard.instantiateViewController(withIdentifier:controllerIdentifier)
        vc.navigationController?.pushViewController(mainViewController, animated: true)
    }
    
    
    
    static func showAlert(vc:UIViewController,title:String,msg:String,clickTxt:String)  {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: clickTxt, style: UIAlertAction.Style.default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    
    static func showAlert(vc:UIViewController,title:String,msg:String)  {
        DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1, execute: {
            showAlert(vc: vc, title: title, msg: msg, clickTxt: "Ok")
        })
    }
    
    static func showAlert(vc:UIViewController,msg:String)  {
        DispatchQueue.main.asyncAfter(wallDeadline: DispatchWallTime.now() + 1, execute: {
            showAlert(vc: vc, title: "Error", msg: msg, clickTxt: "Ok")
        })
        
    }
    
    
    static func selectImageFromGallery(vc:UIViewController,imagePicker:UIImagePickerController)  {
        let alert = UIAlertController(title: "Change Photo", message: "Choose Source", preferredStyle: UIAlertController.Style.actionSheet)
        
        alert.addAction(UIAlertAction(title: "Camera", style: .default) { (result : UIAlertAction) -> Void in
            imagePicker.sourceType = .camera
            vc.present(imagePicker, animated: true, completion: nil)
        })
        alert.addAction(UIAlertAction(title: "Photo Gallery", style: .default) { (result : UIAlertAction) -> Void in
            imagePicker.sourceType = .photoLibrary
            vc.present(imagePicker, animated: true, completion: nil)
        })
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { (result : UIAlertAction) -> Void in
        })
        vc.present(alert, animated: true, completion: nil)
    }
    static func textFieldBorder(textField : UIView){
        let line = UIView()
        line.frame.size = CGSize(width: textField.frame.size.width, height: 1)
        line.frame.origin = CGPoint(x: 0, y: textField.frame.maxY - line.frame.height)
        line.backgroundColor = UIColor.darkGray
        line.autoresizingMask = [.flexibleWidth, .flexibleTopMargin]
        textField.addSubview(line)
    }
    
    static func getCurrentValueForLang(str:NumberOfReviews?) -> String {
        if let s = str{
            return s.en
        }
        return ""
    }
    
    static func getAddressFromLatLon(lat: Double, lon: Double,completion: @escaping (ApiResult<String>) -> Void) {
        var center : CLLocationCoordinate2D = CLLocationCoordinate2D()
        
        let ceo: CLGeocoder = CLGeocoder()
        center.latitude = lat
        center.longitude = lon
        
        let loc: CLLocation = CLLocation(latitude:center.latitude, longitude: center.longitude)
        
        
        ceo.reverseGeocodeLocation(loc, completionHandler:
            {(placemarks, error) in
                if (error != nil)
                {
                    print("reverse geodcode fail: \(error!.localizedDescription)")
                }
                let pm = placemarks! as [CLPlacemark]
                
                if pm.count > 0 {
                    let pm = placemarks![0]
                   
                    var addressString : String = ""
                    if pm.subLocality != nil {
                        addressString = addressString + pm.subLocality! + ", "
                    }
                    if pm.thoroughfare != nil {
                        addressString = addressString + pm.thoroughfare! + ", "
                    }
                    if pm.locality != nil {
                        addressString = addressString + pm.locality! + ", "
                    }
                    if pm.country != nil {
                        addressString = addressString + pm.country! + ", "
                    }
                    if pm.postalCode != nil {
                        addressString = addressString + pm.postalCode! + " "
                    }
                    completion(.success(addressString))
                }
        })
        
    }
}
