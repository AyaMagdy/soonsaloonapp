//
//  UserHistoryRequestsTableViewCell.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/13/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

protocol CellProtocolWithIndex {
    func click(position:Int)
}

import UIKit
import Cosmos

class UserHistoryRequestsTableViewCell: UITableViewCell {

    @IBOutlet weak var timeTextField: UILabel!
    @IBOutlet weak var dateTextField: UILabel!
    @IBOutlet weak var descriptionTextField: UILabel!
    @IBOutlet weak var phoneTextField: UILabel!
    @IBOutlet weak var statusTextField: UILabel!
    @IBOutlet weak var nameTextField: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var rateBtn: UIButton!
    @IBOutlet weak var rateView: CosmosView!
    
    var cellProtocol:CellProtocolWithIndex?
    var index = 0

    func setRequest(requests : Request){
        timeTextField.text = requests.time
        dateTextField.text = requests.date
        descriptionTextField.text = requests.requestMessage
        phoneTextField.text = requests.customerPhone
        statusTextField.text = requests.status
        nameTextField.text = requests.customerName
        locationLabel.text = requests.saloonAddress
        rateView.settings.updateOnTouch = false
        if requests.rate == "" {
            rateView.isHidden = true
            rateBtn.isHidden = false
        }
        else{
            rateView.isHidden = false
            rateView.rating = Double(requests.rate) ?? 0
            rateBtn.isHidden = true
        }
    }
    @IBAction func rateAction(_ sender: Any) {
        print("cell")
         cellProtocol?.click(position: index)
    }
}
