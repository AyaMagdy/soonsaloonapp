//
//  AboutViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/28/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit
import WebKit


class AboutViewController: BaseViewController {

 
    @IBOutlet weak var aboutWebView: WKWebView!
    var about = ""
    var refreshControl = UIRefreshControl()
    override func viewDidLoad() {
        super.viewDidLoad()
        getAboutPage()
    }
    
    func getAboutPage()
    {
            showDialog(vc: self)
             RequestAPI.shared.about()
              { result in
                switch result {
                case let .success(about):
                    self.dismissDialog()
                    var headerString = "<header><meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0'></header>"
                    headerString.append(about )
                    self.aboutWebView.loadHTMLString(headerString, baseURL: nil)
                    self.aboutWebView.isHidden = false
//                    self.aboutWebView.stopAnimating()
                    break
                case let .failure(error):
                    self.dismissDialog()
                    Utlities.showAlert(vc: self, msg:error)
                    break
                }
        }
    }
}



