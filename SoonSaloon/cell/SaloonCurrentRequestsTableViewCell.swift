//
//  SaloonCurrentRequestsTableViewCell.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/9/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit
enum actionType {
    case changeStatus
    case edit
    case cancel
}

protocol CellProtocolIndex {
    func click(position:Int , type : actionType )
}

class SaloonCurrentRequestsTableViewCell: UITableViewCell {

    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var statusAction: UIButton!
    @IBOutlet weak var timeTextField: UILabel!
    @IBOutlet weak var dateTextField: UILabel!
    @IBOutlet weak var descriptionTextField: UILabel!
    @IBOutlet weak var phoneTextField: UILabel!
    @IBOutlet weak var nameTextField: UILabel!

    @IBOutlet weak var statusTextField: UILabel!
    
    @IBOutlet var btnCancel: UIButton!
    
    @IBOutlet var btnEdit: UIButton!
    
    var cellProtocol:CellProtocolIndex?
    var index = 0
    
    func setRequest(requests : Request){
        
        timeTextField.text = requests.time
        dateTextField.text = requests.date
        descriptionTextField.text = requests.requestMessage
        phoneTextField.text = requests.customerPhone
        statusTextField.text = requests.status
        nameTextField.text = requests.customerName
        if (requests.statusCode == "\(1)")
        {
            statusAction.backgroundColor = #colorLiteral(red: 0.6107296944, green: 0.1066358462, blue: 0.2373355627, alpha: 1)
            statusAction.setTitle("Accept",for: .normal)
            statusTextField.textColor = #colorLiteral(red: 0.6107296944, green: 0.1066358462, blue: 0.2373355627, alpha: 1)
            btnEdit.isHidden = false
            btnCancel.isHidden = false
        }
        else if requests.statusCode == "\(2)"
        {
          statusAction.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
          statusAction.setTitle("Finish",for: .normal)
          statusTextField.textColor = #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1)
          btnEdit.isHidden = true
          btnCancel.isHidden = true
        }
        
    }
    @IBAction func statusAction(_ sender: Any) {
        cellProtocol?.click(position: index, type: .changeStatus)
        
    }
    @IBAction func cancelAction(_ sender: Any) {
        cellProtocol?.click(position: index, type: .cancel)
    }
    @IBAction func editAction(_ sender: Any) {
        cellProtocol?.click(position: index, type: .edit)
    }
}
