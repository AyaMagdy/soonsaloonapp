//
//  ServiceDetailsViewController.swift
//  SoonSaloon
//
//  Created by aya magdy on 5/6/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import UIKit
import Cosmos
import SDWebImage

class ServiceDetailsViewController: BaseViewController, UICollectionViewDelegateFlowLayout , UICollectionViewDataSource,UICollectionViewDelegate ,UITableViewDelegate ,UITableViewDataSource {
    

    @IBOutlet weak var reviewsTableView: UITableView!
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var mainImageView: UIImageView!
    
    @IBOutlet weak var numberOfReviewsLabel: UILabel!
    @IBOutlet weak var seatsLabel: UILabel!
    @IBOutlet weak var workingDayLabel: UILabel!
    @IBOutlet weak var workingTimeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var serviceRateView: CosmosView!
    var id :Int? = nil
    
    var comments : [Comment] = []
    var serviceImages = [ServiceImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getServiceData()
        serviceRateView.settings.updateOnTouch = false

    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return serviceImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SaloonServiceCell", for: indexPath) as! SaloonServiceCollectionViewCell
        let service = serviceImages[indexPath.row]
        cell.setService(serviceImage : service)
        return cell
    }
    
    // to put 5 photo in cell and take view size
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/5.0
        let yourHeight = collectionView.bounds.height
        return CGSize(width: yourWidth, height: yourHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    // end put 5 photo in cell
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        Utlities.setImage(imageView: mainImageView, url: serviceImages[indexPath.row].image)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reviewsCell" ) as! SaloonDetailsReviewsTableViewCell
        let comment = comments[indexPath.row]
        cell.setData(comment: comment)
        return cell
    }
    
    func getServiceData(){
            showDialog(vc: self)
            RequestAPI.shared.getSaloonSeviceDetails(serviceId: id!)
                { result in
                    switch result {
                    case let .success(service):
                        //if let
                        self.dismissDialog()
                        self.title = service?.saloonName
                        self.serviceImages = ((service?.images)!)
                
                        Utlities.setImage(imageView: self.mainImageView, url: self.serviceImages.first?.image ?? "")

                        self.imagesCollectionView.reloadData()
                        self.serviceRateView.rating = Double((service?.rate)!)
                        self.numberOfReviewsLabel.text = Utlities.getCurrentValueForLang(str: service?.numberOfReviews)
                        self.seatsLabel.text = service?.seats
                        self.priceLabel.text = service?.price
                        self.descriptionLabel.text = service?.description
                        self.workingTimeLabel.text = Constant.workingTime + ":" + (service?.openingTimeFrom)! + " \(Constant.to) " + (service?.openingTimeTo)!
                        self.workingDayLabel.text = "\(Constant.workingDay) : \(Constant.Days[service!.openingDayFrom]) - \(Constant.Days[service!.openingDayTo])"
                        self.comments = service?.comments ?? []
                        self.reviewsTableView.reloadData()
                        break
                    case let .failure(error):
                        print("fail")
                        self.dismissDialog()
                        Utlities.showAlert(vc: self, msg:error)
                        break
                    }
            }
         }
}
