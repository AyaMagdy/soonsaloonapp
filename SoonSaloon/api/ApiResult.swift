//
//  ApiResult.swift
//  SoonSaloon
//
//  Created by aya magdy on 4/30/19.
//  Copyright © 2019 aya magdy. All rights reserved.
//

import Foundation

enum ApiResult<Value> {
    case success(Value)
    case failure(String)
}
